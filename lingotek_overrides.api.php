<?php

/**
 * @file
 * Hooks provided by the Lingotek Overrides module.
 */

/**
 * Allows modules to alter the Lingotek form-filter plugins.
 *
 * @param array $info
 *   The array of plugin info.
 */
function hook_lingotek_overrides_form_filter_alter(array &$info) {

}

/**
 * Allows modules to alter the Lingotek form-ield plugins.
 *
 * @param array $info
 *   The array of plugin info.
 */
function hook_lingotek_overrides_form_field_alter(array &$info) {

}

/**
 * Allows modules to alter the Lingotek form-operation plugins.
 *
 * @param array $info
 *   The array of plugin info.
 */
function hook_lingotek_overrides_form_operation_alter(array &$info) {

}

/**
 * Allows modules to alter the Lingotek form-option plugins.
 *
 * @param array $info
 *   The array of plugin info.
 */
function hook_lingotek_overrides_form_option_alter(array &$info) {

}

/**
 * Allows modules to alter the Lingotek source plugins.
 *
 * @param array $info
 *   The array of plugin info.
 */
function hook_lingotek_overrides_source_alter(array &$info) {

}
