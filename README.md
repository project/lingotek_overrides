# Lingotek Overrides

This module overrides the [Lingotek][lingotek] module to provide additional functionality, correct existing functionality, and overhaul the administrative user interface.

## Functional enhancements

### `lingotek_content_metadata` entity type

The `LingotekContentMetadata` class is overridden specifically to allow Lingotek to play better with the [TMGMT][tmgmt] module, by exposing the entity as translatable (even though technically it's not) and by providing a label derived from the attached translatable entity.

### Entity-reference field configuration

Entity-reference fields have a new option called "Always show related entities". Combined with a filter on an entity's translation-management page, it allows recursively listing all nested entities that have the option enabled, down to a default of 100 nesting levels.

## Service overrides

## `Lingotek`, `LingotekApi`

New ability to retrieve information on translation phases (tasks) from Lingotek, download translations as XLIFF data, and upload XLIFF data back to Lingotek.

## `LingotekContentTranslationService`

Adds hooks to allow extracting and saving translatable data from and to field types that aren't covered by the Lingotek module.

## Interface enhancements

The module adds four plugin types that control different aspects of the administrative interface:

+ Filters
+ Bulk operations
+ Options for bulk operations
+ Table columns

The filter plugins work in addition to the existing filters---the reason being that too much code would have to be rewritten to replace all filters.

The bulk-operation plugins, in combination with the option plugins, separate operations from language choices, thus making the interface much more manageable, particularly when many languages are enabled.

The table-column (aka filter) plugins replace all the existing columns and add new ones. They also allow sortable columns.

The only forms on which this plugin system is enabled are `LingotekManagementForm` and `LingotekManagementRelatedEntitiesForm`, on the latter of which it also enables filtering. `LingotekConfigManagementForm` is overridden only to allow proper filtering by label on entity-field configuration entities.

## Compatibility

Lingotek Overrides is compatible with the Lingotek module down to the minor version. For example, version 3.5.x requires Lingotek 3.5.x, regardless of patch number.


[lingotek]: https://drupal.org/project/lingotek
[tmgmt]: https://drupal.org/project/tmgmt
