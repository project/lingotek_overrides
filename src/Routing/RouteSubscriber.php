<?php

namespace Drupal\lingotek_overrides\Routing;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\Core\Routing\RoutingEvents;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * The entity_type.manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * RouteSubscriber constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity_type.manager service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    if ($route = $collection->get('lingotek.entity.manage')) {
      $route->setDefault('_form', 'Drupal\lingotek_overrides\Form\LingotekManagementRelatedEntitiesForm');
    }

    foreach ($this->entityTypeManager->getDefinitions() as $entity_type_id => $entity_type) {
      if ($route = $collection->get("lingotek.manage.{$entity_type_id}")) {
        $route->setDefault('_form', 'Drupal\lingotek_overrides\Form\LingotekManagementForm');
      }
    }

    if ($route = $collection->get('lingotek.manage_config')) {
      $route->setDefault('_form', 'Drupal\lingotek_overrides\Form\LingotekConfigManagementForm');
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events = parent::getSubscribedEvents();
    // ContentTranslationRouteSubscriber is -100.
    $events[RoutingEvents::ALTER] = ['onAlterRoutes', -221];
    return $events;
  }

}
