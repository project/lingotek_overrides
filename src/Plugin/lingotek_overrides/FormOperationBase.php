<?php

namespace Drupal\lingotek_overrides\Plugin\lingotek_overrides;

use Drupal\Core\Form\FormStateInterface;
use Exception;

/**
 * Base class for Lingotek form-operation plugins.
 *
 * @package Drupal\lingotek_overrides\Plugin\lingotek_overrides
 */
abstract class FormOperationBase extends FormPluginBase implements FormOperationInterface {

  /**
   * {@inheritdoc}
   */
  public function callback(FormStateInterface $form_state) {
    // Let LingotekManagementFormBase::submitForm() do its job.
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function execute($method, ...$arguments) {
    if (!method_exists($this, $method)) {
      throw new Exception(sprintf("The method '%s' does not exist.", $method));
    }

    $this->{$method}($arguments);
  }

  /**
   * {@inheritdoc}
   */
  public function getOptions() {
    return $this->pluginDefinition['options'] ?? [];
  }

  /**
   * Returns the form's submitted values.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The form's values.
   */
  protected function getValues(FormStateInterface $form_state) {
    return array_keys(array_filter($form_state->getValue(['table'])));
  }

  /**
   * Returns the selected languages.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The selected languages.
   */
  protected function getLanguages(FormStateInterface $form_state) {
    return array_filter($form_state->getValue('language'));
  }

}
