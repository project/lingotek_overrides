<?php

namespace Drupal\lingotek_overrides\Plugin\lingotek_overrides;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for Lingotek form-option plugins.
 *
 * @package Drupal\lingotek_overrides\Plugin\lingotek_overrides
 */
abstract class FormOptionBase extends FormPluginBase implements FormOptionInterface {

  /**
   * The operations that are allowing the plugin.
   *
   * Set by the plugin manager when the plugin instance is created.
   *
   * @var \Drupal\lingotek_overrides\Plugin\lingotek_overrides\FormOperationInterface[]
   */
  protected $operations = [];

  /**
   * The lingotek.management.option private tempstore.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStore
   */
  protected $tempStore;

  /**
   * FormPluginBase constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity_type.manager service.
   * @param \Drupal\Core\Database\Connection $connection
   *   The database service.
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $temp_store_factory
   *   The tempstore.private service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    Connection $connection,
    PrivateTempStoreFactory $temp_store_factory
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $connection);
    $this->tempStore = $temp_store_factory->get('lingotek.management.option');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('database'),
      $container->get('tempstore.private')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildElement(array $element = []) {
    $element['#element_validate'][] = [$this, 'validateElement'];

    if (!isset($element['#default_value'])) {
      // Defaults to empty array when null, to avoid errors when rendering
      // checkboxes.
      $element['#default_value'] = $this->tempStore->get($this->pluginId) ?? [];
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function validateElement(array &$element, FormStateInterface $form_state) {
    // Validate the element if the chosen operation supports this option.
    if ($this->allowValidation($element, $form_state)) {
      $option = $form_state->getValue($this->pluginId);

      if (is_array($option)) {
        $option = array_filter($option);
      }

      if (!$option) {
        $form_state->setError($element, $this->getErrorMessage());
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function setOperations(array $operations) {
    $this->operations = $operations;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOperations() {
    return $this->operations;
  }

  /**
   * {@inheritdoc}
   */
  public function getValueKeys() {
    return [$this->pluginId];
  }

  /**
   * Validates the element if the chosen operation supports this option.
   *
   * @param array $element
   *   The form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return bool
   *   Whether validation is allowed.
   */
  protected function allowValidation(array &$element, FormStateInterface $form_state) {
    if ($operation = $form_state->getValue('operation') ?? NULL) {
      return isset($this->operations[$operation]);
    }

    return FALSE;
  }

  /**
   * Returns a validation-error message.
   *
   * Plugins should customize this message with more specific wording.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The error message.
   */
  protected function getErrorMessage() {
    return $this->t('Select an option.');
  }

}
