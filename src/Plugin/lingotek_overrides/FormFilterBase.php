<?php

namespace Drupal\lingotek_overrides\Plugin\lingotek_overrides;

use Drupal\Core\Database\Query\SelectInterface;

/**
 * Base class for Lingotek form-filter plugins.
 *
 * @package Drupal\lingotek_overrides\Plugin\lingotek_overrides
 */
abstract class FormFilterBase extends FormPluginBase implements FormFilterInterface {

  /**
   * {@inheritdoc}
   */
  public function getFilterKey() {
    return [$this->pluginDefinition['id']];
  }

  /**
   * {@inheritdoc}
   */
  public function filter(string $entity_type, array $entities, $value, SelectInterface &$query = NULL) {
    if (!$query || !$query->hasTag('lingotek_overrides_form_filter_base')) {
      $entity_type = $this->entityTypeManager->getDefinition($entity_type);
      $entity_id_key = $entity_type->getKey('id');

      if (!$query) {
        $query = $this->connection->select($entity_type->getBaseTable(), 'entity_table')
          ->fields('entity_table', [$entity_id_key])
          // Make sure the query has at least two fields.
          ->fields('metadata_source', ['content_entity_type_id'])
          ->condition("entity_table.$entity_id_key", array_keys($entities), 'IN');
        $this->joinMetadata($query, $entity_id_key);
      }
      else {
        $tables = $query->getTables();

        if (!isset($tables['metadata_source'])) {
          $this->joinMetadata($query, $entity_id_key);
        }
      }

      // Include entities that don't have Lingotek metadata.
      $l_content_entity_type_group = $query->orConditionGroup();
      $l_content_entity_type_group->condition('metadata_source.content_entity_type_id', $entity_type->id());
      $l_content_entity_type_group->condition('metadata_source.content_entity_type_id', NULL, 'IS NULL');
      $query->condition($l_content_entity_type_group);
      $query->addTag('lingotek_overrides_form_filter_base');
    }

    return $query;
  }

  /**
   * Adds left join on the lingotek_metadata table.
   *
   * @param \Drupal\Core\Database\Query\SelectInterface $query
   *   The query.
   * @param string $entity_id_key
   *   The entity ID's key.
   */
  protected function joinMetadata(SelectInterface $query, string $entity_id_key) {
    $query->leftJoin('lingotek_metadata', 'metadata_source', "metadata_source.content_entity_id = entity_table.$entity_id_key");
  }

}
