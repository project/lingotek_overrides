<?php

namespace Drupal\lingotek_overrides\Plugin\lingotek_overrides;

use Drupal\Core\Database\Query\SelectInterface;

/**
 * Interface for Lingotek form-filter plugins.
 *
 * @package Drupal\lingotek_overrides\Plugin\lingotek_overrides
 */
interface FormFilterInterface extends FormPluginInterface {

  /**
   * Builds the filter element.
   *
   * @param mixed|null $default_value
   *   The filter's default value.
   *
   * @return array
   *   The element's renderable array.
   */
  public function buildElement($default_value = NULL);

  /**
   * Returns the filter-key array.
   *
   * @return array
   *   The filter key.
   *
   * @see \Drupal\lingotek\Form\LingotekManagementFormBase::getFilterKeys()
   */
  public function getFilterKey();

  /**
   * Performs the filter operation.
   *
   * @param string $entity_type
   * @param \Drupal\Core\Entity\EntityInterface[] $entities
   * @param mixed $value
   * @param \Drupal\Core\Database\Query\SelectInterface|null $query
   *
   * @return \Drupal\Core\Database\Query\SelectInterface|null
   */
  public function filter(string $entity_type, array $entities, $value, SelectInterface &$query = NULL);

}
