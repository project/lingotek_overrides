<?php

namespace Drupal\lingotek_overrides\Plugin\lingotek_overrides;

use Drupal\Core\Form\FormStateInterface;

/**
 * Interface for Lingotek form-operation plugins.
 */
interface FormOperationInterface extends FormPluginInterface {

  /**
   * Gets the operation's options, if any.
   *
   * @return array
   *   The options' plugin IDs.
   */
  public function getOptions();

  /**
   * Returns an array of submit callback and arguments.
   *
   * Each returned item should be an array whose first item is a valid callback,
   * followed by a variable number of arguments. If the callback is a string,
   * it will be considered a method of LingotekManagementFormBase.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The callback's method name and arguments.
   */
  public function callback(FormStateInterface $form_state);

  /**
   * Calls a protected method on the given arguments.
   *
   * @param string $method
   *   The protected method's name.
   * @param misc ...$arguments
   *   A variable number of arguments.
   */
  public function execute(string $method, ...$arguments);

}
