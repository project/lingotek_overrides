<?php

namespace Drupal\lingotek_overrides\Plugin\lingotek_overrides\FormFilter;

use Drupal\Core\Database\Query\SelectInterface;
use Drupal\lingotek_overrides\Plugin\lingotek_overrides\FormFilterBase;

/**
 * Defines a Lingotek form-filter plugin for the Lingotek job ID.
 *
 * @LingotekOverridesFormFilter(
 *   id = "lingotek_overrides_job_id",
 *   title = @Translation("Job ID"),
 *   weight = -2000,
 *   form_ids = {
 *     "lingotek_management",
 *     "lingotek_entity_management",
 *   }
 * )
 */
class JobId extends FormFilterBase {

  /**
   * {@inheritdoc}
   */
  public function buildElement($default_value = NULL) {
    return [
      '#type' => 'lingotek_job_id',
      '#title' => $this->t('Job ID'),
      '#description' => $this->t('You can indicate multiple comma-separated values.<br />The prefix "not:" will return entities with any job ID except the listed ones.<br />Entering <code>&lt;none&gt;</code> will return entities without a job ID.<br />Entering <code>&lt;any&gt;</code> will return entities with any job ID.'),
      '#default_value' => $default_value,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function filter(string $entity_type, array $entities, $value, SelectInterface &$query = NULL) {
    parent::filter($entity_type, $entities, $value, $query);

    if ($value = trim($value)) {
      if (!$query) {
        $query = parent::filter($entity_type, $entities, $value);
      }

      switch ($value) {
        case '<any>':
          $query->condition('metadata_source.job_id', NULL, 'IS NOT NULL');
          break;

        case '<none>':
          $query->condition('metadata_source.job_id', NULL, 'IS NULL');
          break;

        default:
          $operator = 'IN';
          $prefix = 'not:';

          if (substr($value, 0, 4) == $prefix) {
            $value = trim(str_replace($prefix, '', $value));
            $operator = 'NOT IN';
          }

          $value = explode(',', $value);
          array_walk($value, function (&$item) {
            $item = trim($item);
          });

          $query->condition('metadata_source.job_id', $value, $operator);
      }
    }
  }

}
