<?php

namespace Drupal\lingotek_overrides\Plugin\lingotek_overrides\FormFilter;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Query\SelectInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\lingotek_overrides\LingotekConfigurationServiceInterface;
use Drupal\lingotek_overrides\Plugin\lingotek_overrides\FormFilterBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a Lingotek form-filter plugin for the entity type and bundle
 *
 * @LingotekOverridesFormFilter(
 *   id = "lingotek_overrides_entity_bundle",
 *   title = @Translation("Bundle"),
 *   form_ids = {
 *     "lingotek_entity_management",
 *   }
 * )
 */
class EntityBundle extends FormFilterBase {

  /**
   * The current_route_match service.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $currentRoute;

  /**
   * The lingotek.configuration service.
   *
   * @var \Drupal\lingotek_overrides\LingotekConfigurationServiceInterface
   */
  protected $lingotekConfiguration;

  /**
   * The entity_field.manager service.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The lingotek.management.filter.node private tempstore.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStore
   */
  protected $filterTempStore;

  /**
   * Defines whether the filter is limited to restricted fields.
   *
   * @var bool
   */
  protected $restrict;

  /**
   * Entity types used in the filter.
   *
   * @var \Drupal\Core\Entity\EntityTypeInterface[]
   */
  protected $entityTypes;

  /**
   * An array of available bundle definitions, keyed by entity type and bundle.
   *
   * @var array[]
   */
  protected $bundleDefinitions;

  /**
   * An array of filterable bundle names, keyed by entity-type ID.
   *
   * @var array
   */
  protected $bundles = [];

  /**
   * An array of visited bundle names, keyed by (field name and) entity-type ID.
   *
   * @var array
   */
  protected $visited = [];

  /**
   * FormPluginBase constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity_type.manager service.
   * @param \Drupal\Core\Database\Connection $connection
   *   The database service.
   * @param \Drupal\Core\Routing\RouteMatchInterface $current_route
   *   The current_route_match service.
   * @param \Drupal\lingotek_overrides\LingotekConfigurationServiceInterface $lingotek_configuration
   *   The lingotek.configuration service.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity_field.manager service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    Connection $connection,
    RouteMatchInterface $current_route,
    LingotekConfigurationServiceInterface $lingotek_configuration,
    EntityFieldManagerInterface $entity_field_manager,
    PrivateTempStoreFactory $temp_store_factory
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $connection);
    $this->currentRoute = $current_route;
    $this->lingotekConfiguration = $lingotek_configuration;
    $this->entityFieldManager = $entity_field_manager;
    $this->filterTempStore = $temp_store_factory->get('lingotek.management.filter.node');
    $this->restrict = $this->getRestrictRelated();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('database'),
      $container->get('current_route_match'),
      $container->get('lingotek.configuration'),
      $container->get('entity_field.manager'),
      $container->get('tempstore.private')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildElement($default_value = NULL) {
    $options = [];
    $extract = $this->extract();

    foreach ($extract as $entity_type_id => $bundles) {
      $_options = [];
      $bundles = array_intersect_key($this->bundleDefinitions[$entity_type_id], array_flip($bundles));

      foreach ($bundles as $bundle_name => $bundle) {
        $_options["$entity_type_id:$bundle_name"] = $bundle['label'];
      }

      $entity_type = $this->entityTypes[$entity_type_id];
      $entity_type_label = $entity_type->getLabel();
      if (count($bundles) > 1) {
        $_options = array_merge(["$entity_type_id:_any" => $this->t('- @type: Any -', ['@type' => $entity_type_label])], $_options);
      }
      $options[(string) $entity_type_label] = $_options;
    }

    return [
      '#type' => 'select',
      '#title' => $this->pluginDefinition['title'],
      '#default_value' => $default_value,
      '#options' => $options,
      '#empty_option' => $this->t('- Any -'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function filter(string $entity_type, array $entities, $value, SelectInterface &$query = NULL) {
    parent::filter($entity_type, $entities, $value, $query);
    /**
     * @var string $entity_type_id
     * @var string $bundle
     */
    [$entity_type_id, $bundle] = explode(':', $value);

    if ($entity_type_id !== $entity_type) {
      if ($entity_type_definition = $this->entityTypes[$entity_type]) {
        // We need an impossible filter to prevent all results.
        $id_key = $entity_type_definition->getKey('id');
        $query->condition("entity_table.$id_key", 0, '<');
      }
    }
    else {
      if ($entity_type_definition = $this->entityTypes[$entity_type_id]) {
        // Do nothing if the entity type doesn't have bundles.
        if ($entity_type_definition->getBundleEntityType()) {
          $bundle_key = $entity_type_definition->getKey('bundle');

          if ($bundle !== '_any') {
            $query->condition("entity_table.$bundle_key", $bundle);
          }
        }
      }
    }
  }

  /**
   * Extracts filterable entity types and bundles from the current entity.
   *
   * @param string $entity_type_id
   *   The entity-type ID.
   * @param string $entity_bundle
   *   The bundle.
   * @param string $field_name
   *   The name of the field that's referencing the bundle.
   * @param int $depth
   *   The recursion depth.
   *
   * @return array
   *   The filterable bundles.
   */
  protected function extract(string $entity_type_id = NULL, string $entity_bundle = NULL, string $field_name = NULL, int $depth = 100) {
    if (!$entity_type_id && !$entity_bundle) {
      /** @var \Drupal\node\NodeInterface $entity */
      if (!($entity = $this->currentRoute->getParameter('node'))) {
        return [];
      }

      $entity_type_id = $entity->getEntityTypeId();
      $entity_bundle = $entity->bundle();
      $this->setEntityType($entity->getEntityType());
      // Store bundle information for later.
      $this->getBundleInfo($entity_type_id);
    }

    $config = $this->lingotekConfiguration;
    // Adding the field name (when available) into the mix allows taking into account the same
    // bundle when referenced in different fields that might have different
    // restriction settings.
    $parents = [
      $entity_type_id,
      $entity_bundle,
    ];

    if ($field_name) {
      $parents = array_merge([$field_name], $parents);
    }

    if (!NestedArray::keyExists($this->visited, $parents)) {
      NestedArray::setValue($this->visited, $parents, $entity_bundle);

      if ($config->isEnabled($entity_type_id, $entity_bundle)) {
        if (!isset($this->bundles[$entity_type_id]) || !in_array($entity_bundle, $this->bundles[$entity_type_id])) {
          $this->bundles[$entity_type_id][] = $entity_bundle;
        }
      }
    }

    if ($depth > 0) {
      --$depth;
      $field_definitions = $this->entityFieldManager->getFieldDefinitions($entity_type_id, $entity_bundle);

      foreach ($field_definitions as $field_name => $definition) {
        if ($definition->isComputed()) {
          continue;
        }

        $field_type = $definition->getType();
        $field_restricted = FALSE;

        if (in_array($field_type, ['entity_reference', 'entity_reference_revisions'])) {
          if ($this->restrict) {
            if (!$config->getSetting('manage_related_show', $entity_type_id, $entity_bundle, $field_name)) {
              $field_restricted = TRUE;
            }
          }

          $target_entity_type_id = $definition->getFieldStorageDefinition()
            ->getSetting('target_type');
          $target_entity_type = $this->entityTypeManager->getDefinition($target_entity_type_id);
          $this->setEntityType($target_entity_type);

          $implements = class_implements($target_entity_type->getClass());

          if (in_array('Drupal\Core\Entity\ContentEntityInterface', $implements)) {
            $all_target_bundles = $this->getBundleInfo($target_entity_type_id);

            if (!($all_target_bundles = array_filter($all_target_bundles, function($_target_bundle) {
              return !empty($_target_bundle['translatable']);
            }))) {
              continue;
            }

            if ($target_bundles = $definition->getSetting('handler_settings')['target_bundles'] ?? []) {
              $target_bundles = array_intersect_key($all_target_bundles, array_flip($target_bundles));
            }
            else {
              $target_bundles = $all_target_bundles;
            }

            foreach ($target_bundles as $target_bundle => $target_bundle_definition) {
              if (!isset($this->bundleDefinitions[$target_entity_type_id][$target_bundle])) {
                $this->bundleDefinitions[$target_entity_type_id][$target_bundle] = $target_bundle_definition;
              }

              if (!$this->restrict) {
                if (!$config->isEnabled($target_entity_type_id, $target_bundle)) {
                  continue;
                }
              }

              $parents = [
                $field_name,
                $target_entity_type_id,
                $target_bundle,
              ];

              if (!NestedArray::keyExists($this->visited, $parents)) {
                if ($field_restricted) {
                  // The field's entities shouldn't be displayed, but some
                  // of their (recursive) children might have to.
                  // $this->visited[$field_name][$target_entity_type_id][] = $target_bundle;
                  NestedArray::setValue($this->visited, $parents, $target_bundle);
                }

                $this->bundles += $this->extract($target_entity_type_id, $target_bundle, $field_name, $depth);
              }
            }
          }
        }
      }
    }

    return $this->bundles;
  }

  /**
   * Checks whether the restrict filter is active.
   *
   * @return bool
   *   If TRUE, the restrict filter is active.
   *
   * @see \Drupal\lingotek_overrides\Form\LingotekManagementRelatedEntitiesForm::getRestrictRelated()
   */
  protected function getRestrictRelated() {
    static $restrict;

    if (is_null($restrict)) {
      $restrict = $this->filterTempStore->get('restrict_related');

      if (is_null($restrict)) {
        $restrict = TRUE;
      }
      else {
        $restrict = (bool) $restrict;
      }
    }

    return $restrict;
  }

  /**
   * Saves an entity type's definition to a local property.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity-type definition.
   */
  protected function setEntityType(EntityTypeInterface $entity_type) {
    $id = $entity_type->id();

    if (!isset($this->entityTypes[$id])) {
      $this->entityTypes[$id] = $entity_type;
    }
  }

  /**
   * Retrieves bundle information for a given entity type.
   *
   * Returns only translatable bundles.
   *
   * @param string $entity_type_id
   *   The entity-type ID.
   *
   * @return array
   *   An array of bundle definitions.
   */
  protected function getBundleInfo(string $entity_type_id) {
    if (!isset($this->bundleDefinitions[$entity_type_id])) {
      // TODO: inject dependency
      $bundle_definitions = \Drupal::service('entity_type.bundle.info')->getBundleInfo($entity_type_id);

      $this->bundleDefinitions[$entity_type_id] = array_filter($bundle_definitions, function ($definition) {
        return !empty($definition['translatable']);
      });
    }

    return $this->bundleDefinitions[$entity_type_id];
  }

}
