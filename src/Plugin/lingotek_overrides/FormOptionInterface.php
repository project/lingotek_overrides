<?php

namespace Drupal\lingotek_overrides\Plugin\lingotek_overrides;

use Drupal\Core\Form\FormStateInterface;

/**
 * Interface for Lingotek form-option plugins.
 */
interface FormOptionInterface extends FormPluginInterface {

  /**
   * Builds the option's form element.
   *
   * @param array $element
   *   The existing form element, if any.
   *
   * @return array
   *   The form element.
   */
  public function buildElement(array $element = []);

  /**
   * Validates the option element.
   *
   * @param array $element
   *   The form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function validateElement(array &$element, FormStateInterface $form_state);

  /**
   * Stores the operations that are allowing the plugin.
   *
   * @param \Drupal\lingotek_overrides\Plugin\lingotek_overrides\FormOperationInterface[] $operations
   *   The operations.
   *
   * @return $this
   */
  public function setOperations(array $operations);

  /**
   * Retrieves the operations that are allowing the plugin.
   *
   * @return \Drupal\lingotek_overrides\Plugin\lingotek_overrides\FormOperationInterface[]
   *   The operations.
   */
  public function getOperations();

  /**
   * Gets the option's value keys.
   *
   * Useful if the option is setting more than one form element.
   *
   * @return array
   *   The value keys to be retrieved from the form state.
   */
  public function getValueKeys();

}
