<?php

namespace Drupal\lingotek_overrides\Plugin\lingotek_overrides;

/**
 * Base class for Lingotek form-field plugins.
 *
 * @package Drupal\lingotek_overrides\Plugin\lingotek_overrides
 */
abstract class FormFieldBase extends FormPluginBase implements FormFieldInterface {

  /**
   * Returns a weighted table-ready array.
   *
   * @param $value
   *
   * @return array
   */
  protected function weight($value) {
    return [
      'data' => $value,
      'weight' => $this->pluginDefinition['weight'],
    ];
  }

  /**
   * Provides sort parameters for the header.
   *
   * @param string $entity_type_id
   *   The entity-type ID.
   *
   * @return array
   *   An array of sort parameters:
   *   - field: the field by which the query will be sorted,
   *     e.g. 'entity_data.nid'.
   *   - sort (optional): the sort direction. Defaults to 'asc'.
   */
  protected function sort($entity_type_id) {
    return [];
  }

}
