<?php

namespace Drupal\lingotek_overrides\Plugin\lingotek_overrides;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Generic interface for Lingotek form plugins.
 */
interface FormPluginInterface extends PluginInspectionInterface {

  /**
   * Establishes whether the plugin is applicable to the form.
   *
   * @param array $arguments
   *   The arguments.
   *
   * @return bool
   *   If TRUE, the plugin is applicable.
   */
  public function isApplicable(array $arguments = []);

  /**
   * Sets the entity-type ID, if any.
   *
   * @param string|null $entity_type_id
   *   The entity-type ID.
   *
   * @return $this
   */
  public function setEntityTypeId(?string $entity_type_id);

  /**
   * Gets the plugin's title.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The title.
   */
  public function getTitle();

  /**
   * Gets the plugin's group, if any.
   *
   * @return string|null
   *   The group.
   */
  public function getGroup();

  /**
   * Gets the plugin's weight.
   *
   * @return int
   *   The weight.
   */
  public function getWeight();

}
