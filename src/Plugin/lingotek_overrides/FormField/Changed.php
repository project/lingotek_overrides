<?php

namespace Drupal\lingotek_overrides\Plugin\lingotek_overrides\FormField;

use Drupal\Core\Database\Connection;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\lingotek_overrides\Plugin\lingotek_overrides\FormFieldBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a Lingotek form-field plugin for an entity's changed date.
 *
 * @LingotekOverridesFormField(
 *   id = "lingotek_overrides_changed",
 *   title = @Translation("Last modified"),
 *   weight = 999,
 *   form_ids = {
 *     "lingotek_management",
 *     "lingotek_entity_management",
 *   }
 * )
 */
class Changed extends FormFieldBase {

  /**
   * Whether this plugin is allowed.
   *
   * @var bool|null
   */
  protected static $allowed;

  /**
   * The date.formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * Changed constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity_type.manager service.
   * @param \Drupal\Core\Database\Connection $connection
   *   The database service.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date.formatter service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, Connection $connection, DateFormatterInterface $date_formatter) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $connection);
    $this->dateFormatter = $date_formatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('database'),
      $container->get('date.formatter')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getHeader($entity_type_id = NULL) {
    if ($entity_type_id) {
      if (is_null(static::$allowed)) {
        $definition = $this->entityTypeManager->getDefinition($entity_type_id);
        // Check whether the entity class uses EntityChangedTrait.
        static::$allowed = method_exists($definition->getClass(), 'setChangedTime');
      }

      if (static::$allowed === TRUE) {
        return array_merge($this->weight($this->pluginDefinition['title']), $this->sort($entity_type_id));
      }
    }

    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getData(EntityInterface $entity) {
    if (static::$allowed === TRUE) {
      return $this->weight($this->dateFormatter->format($entity->getChangedTimeAcrossTranslations(), 'short'));
    }

    return [];
  }

  /**
   * {@inheritdoc}
   */
  protected function sort($entity_type_id) {
    return [
      'field' => 'entity_data.changed',
      'sort' => 'desc',
    ];
  }

}
