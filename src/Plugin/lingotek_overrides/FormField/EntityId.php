<?php

namespace Drupal\lingotek_overrides\Plugin\lingotek_overrides\FormField;

use Drupal\Core\Entity\EntityInterface;
use Drupal\lingotek_overrides\Plugin\lingotek_overrides\FormFieldBase;

/**
 * Defines a Lingotek form-field plugin for an entity ID.
 *
 * @LingotekOverridesFormField(
 *   id = "lingotek_overrides_entity_id",
 *   title = @Translation("Entity ID"),
 *   weight = -2000,
 *   form_ids = {
 *     "lingotek_management",
 *     "lingotek_entity_management",
 *   }
 * )
 */
class EntityId extends FormFieldBase {

  /**
   * {@inheritdoc}
   */
  public function getHeader($entity_type_id = NULL) {
    return array_merge($this->weight($this->t('ID')), $this->sort($entity_type_id));
  }

  /**
   * {@inheritdoc}
   */
  public function getData(EntityInterface $entity) {
    return $this->weight($entity->id());
  }

  /**
   * {@inheritdoc}
   */
  protected function sort($entity_type_id) {
    if ($entity_type_id) {
      $entity_type = $this->entityTypeManager->getDefinition($entity_type_id);

      return [
        'field' => 'entity_data.' . $entity_type->getKey('id'),
      ];
    }

    return [];
  }

}
