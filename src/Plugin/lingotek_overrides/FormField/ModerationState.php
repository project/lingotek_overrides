<?php

namespace Drupal\lingotek_overrides\Plugin\lingotek_overrides\FormField;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\TranslatableInterface;
use Drupal\Core\Plugin\Context\Context;

/**
 * Defines a Lingotek form-field plugin for the moderation state.
 *
 * Applies to the entity's latest revision.
 *
 * @LingotekOverridesFormField(
 *   id = "lingotek_overrides_moderation_state",
 *   title = @Translation("Latest revision"),
 *   weight = 951,
 *   form_ids = {
 *     "lingotek_management",
 *     "lingotek_entity_management",
 *   }
 * )
 */
class ModerationState extends Status {

  /**
   * {@inheritdoc}
   */
  public function isApplicable(array $arguments = []) {
    return !empty($this->moderationInformation);
  }

  /**
   * {@inheritdoc}
   */
  public function getHeader($entity_type_id = NULL) {
    return $this->weight($this->getPluginDefinition()['title']);
  }

  /**
   * {@inheritdoc}
   */
  public function getData(EntityInterface $entity) {
    $statuses = [];

    if ($entity instanceof TranslatableInterface) {
      $languages = $this->languageManager->getLanguages();

      foreach ($languages as $langcode => $language) {
        $language_context = Context::createFromContext($this->languageContext, $language);
        /** @var \Drupal\Core\Entity\ContentEntityInterface $active */
        $active = $this->entityRepository->getActive($entity->getEntityTypeId(), $entity->id(), [$this::CURRENT_LANGUAGE_CONTEXT_ID => $language_context]);
        // Make sure the active entity is not a language fallback.
        if ($active->language()->getId() == $langcode) {
          $statuses[$this->getStatus($active)][] = $langcode;
        }
      }
    }

    return $this->weight($this->buildStatuses($statuses));
  }

}
