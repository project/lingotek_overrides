<?php

namespace Drupal\lingotek_overrides\Plugin\lingotek_overrides\FormField;

use Drupal\content_moderation\ModerationInformationInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\TranslatableInterface;
use Drupal\Core\Entity\TranslatableRevisionableStorageInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Language\ContextProvider\CurrentLanguageContext;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Plugin\Context\Context;
use Drupal\lingotek_overrides\Plugin\lingotek_overrides\FormFieldBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException;

/**
 * Defines a Lingotek form-field plugin for an entity's publication status.
 *
 * @LingotekOverridesFormField(
 *   id = "lingotek_overrides_status",
 *   title = @Translation("Status"),
 *   weight = 950,
 *   form_ids = {
 *     "lingotek_management",
 *     "lingotek_entity_management",
 *   }
 * )
 */
class Status extends FormFieldBase {

  /**
   * The ID of the current-language context.
   */
  const CURRENT_LANGUAGE_CONTEXT_ID = '@language.current_language_context:' . LanguageInterface::TYPE_CONTENT;

  /**
   * The entity.repository service.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entityRepository;

  /**
   * The language_manager service.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The current-language context.
   *
   * @var \Drupal\Core\Plugin\Context\ContextInterface
   */
  protected $languageContext;

  /**
   * The module_handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The content_moderation.moderation_information service.
   *
   * @var \Drupal\content_moderation\ModerationInformationInterface
   */
  protected $moderationInformation;

  /**
   * Status constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity_type.manager service.
   * @param \Drupal\Core\Database\Connection $connection
   *   The database service.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language_manager service.
   * @param \Drupal\Core\Language\ContextProvider\CurrentLanguageContext $language_context_provier
   *   The language.current_language_context service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module_handler service.
   * @param \Drupal\content_moderation\ModerationInformationInterface $moderation_information
   *   The content_moderation.moderation_information service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    Connection $connection,
    EntityRepositoryInterface $entity_repository,
    LanguageManagerInterface $language_manager,
    CurrentLanguageContext $language_context_provider,
    ModuleHandlerInterface $module_handler,
    ModerationInformationInterface $moderation_information = NULL
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $connection);
    $this->entityRepository = $entity_repository;
    $this->languageManager = $language_manager;
    $language_contexts = $language_context_provider->getAvailableContexts();
    $this->languageContext = reset($language_contexts);
    $this->moduleHandler = $module_handler;
    $this->moderationInformation = $moderation_information;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    try {
      $moderation_information = $container->get('content_moderation.moderation_information');
    }
    catch (ServiceNotFoundException $e) {
      $moderation_information = NULL;
    }

    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('database'),
      $container->get('entity.repository'),
      $container->get('language_manager'),
      $container->get('language.current_language_context'),
      $container->get('module_handler'),
      $moderation_information
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getHeader($entity_type_id = NULL) {
    if (!$this->moderationInformation) {
      return $this->weight($this->getPluginDefinition()['title']);
    }

    return $this->weight($this->t('Current revision'));
  }

  /**
   * {@inheritdoc}
   */
  public function getData(EntityInterface $entity) {
    $statuses = [];

    if ($entity instanceof TranslatableInterface) {
      $storage = $this->entityTypeManager->getStorage($entity->getEntityTypeId());

      if ($storage instanceof TranslatableRevisionableStorageInterface) {
        $languages = $this->languageManager->getLanguages();

        foreach ($languages as $langcode => $language) {
          $language_context = Context::createFromContext($this->languageContext, $language);
          $canonical = $this->entityRepository->getCanonical($entity->getEntityTypeId(), $entity->id(), [$this::CURRENT_LANGUAGE_CONTEXT_ID => $language_context]);
          if ($canonical->language()->getId() == $langcode) {
            $statuses[$this->getStatus($canonical)][] = $langcode;
          }
        }
      }
    }

    return $this->weight($this->buildStatuses($statuses));
  }

  /**
   * Gets an entity's status or moderation state.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity.
   *
   * @return string
   *   The label of the status or moderation state.
   */
  protected function getStatus(ContentEntityInterface $entity) {
    if ($entity instanceof EntityPublishedInterface) {
      if (!$this->moderationInformation) {
        return $entity->isPublished() ? $this->t('Published')->__toString() : $this->t('Unpublished')->__toString();
      }

      return $this->getModerationState($entity);
    }

    return '';
  }

  /**
   * Gets the revision's content moderation state, if available.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity revision.
   *
   * @return string|bool
   *   Returns the label of the moderation state, if available, otherwise FALSE.
   */
  protected function getModerationState(ContentEntityInterface $entity) {
    if ($this->moderationInformation && $this->moderationInformation->isModeratedEntity($entity)) {
      if ($state = $entity->moderation_state->value) {
        $workflow = $this->moderationInformation->getWorkflowForEntity($entity);
        return $workflow->getTypePlugin()->getState($state)->label();
      }
    }

    return FALSE;
  }

  /**
   * Builds the statuses' renderable array.
   *
   * @param array $statuses
   *   An array of language codes, keyed by publication status or moderation
   *   state.
   *
   * @return array
   *   The renderable array.
   */
  protected function buildStatuses(array $statuses) {
    if ($statuses) {
      $list['#theme'] = 'item_list';

      foreach ($statuses as $status => $langcodes) {
        if ($status) {
          $parameters = ['@status' => $status, '%langcodes' => implode(', ', $langcodes)];
          $list['#items'][] = $this->t('@status: %langcodes', $parameters);
        }
      }
    }

    return $list ?? [];
  }

}
