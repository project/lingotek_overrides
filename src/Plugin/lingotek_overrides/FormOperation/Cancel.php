<?php

namespace Drupal\lingotek_overrides\Plugin\lingotek_overrides\FormOperation;

use Drupal\lingotek_overrides\Plugin\lingotek_overrides\FormOperationBase;

/**
 * Defines a Lingotek plugin for the cancel operation.
 *
 * @LingotekOverridesFormOperation(
 *   id = "cancel",
 *   title = @Translation("Cancel document"),
 *   weight = 80,
 *   form_ids = {
 *     "lingotek_management",
 *     "lingotek_entity_management",
 *   },
 *   group = @Translation("Document")
 * )
 */
class Cancel extends FormOperationBase {

}
