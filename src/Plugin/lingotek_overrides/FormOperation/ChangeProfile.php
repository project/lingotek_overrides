<?php

namespace Drupal\lingotek_overrides\Plugin\lingotek_overrides\FormOperation;

use Drupal\Core\Form\FormStateInterface;
use Drupal\lingotek_overrides\Plugin\lingotek_overrides\FormOperationBase;

/**
 * Defines a Lingotek plugin for the change_profile operation.
 *
 * @LingotekOverridesFormOperation(
 *   id = "change_profile",
 *   title = @Translation("Change translation profile"),
 *   weight = 70,
 *   form_ids = {
 *     "lingotek_management",
 *     "lingotek_entity_management",
 *   },
 *   group = @Translation("Document"),
 *   options = {
 *     "profile",
 *   }
 * )
 */
class ChangeProfile extends FormOperationBase {

  /**
   * {@inheritdoc}
   */
  public function callback(FormStateInterface $form_state) {
    $values = $this->getValues($form_state);

    $callbacks[] = [
      'createChangeProfileBatch',
      $values,
      $form_state->getValue('profile') ?? NULL,
    ];

    return $callbacks;
  }

}
