<?php

namespace Drupal\lingotek_overrides\Plugin\lingotek_overrides\FormOperation;

use Drupal\lingotek_overrides\Plugin\lingotek_overrides\FormOperationBase;

/**
 * Defines a Lingotek plugin for the check_upload operation.
 *
 * @LingotekOverridesFormOperation(
 *   id = "check_upload",
 *   title = @Translation("Check upload progress"),
 *   weight = 20,
 *   form_ids = {
 *     "lingotek_management",
 *     "lingotek_entity_management",
 *   },
 *   group = @Translation("Source")
 * )
 */
class CheckUpload extends FormOperationBase {

}
