<?php

namespace Drupal\lingotek_overrides\Plugin\lingotek_overrides\FormOperation;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\lingotek_overrides\Plugin\lingotek_overrides\FormOperationBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Abstract class for entity-delete operations.
 */
abstract class DeleteOperationBase extends FormOperationBase {

  /**
   * The current_route_match service.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  protected $currentRoute;

  /**
   * The current_user service.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The tempstore.private service.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStoreFactory
   */
  protected $tempstore;

  /**
   * DeleteTranslation constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity_type.manager service.
   * @param \Drupal\Core\Database\Connection $connection
   *   The database service.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current_user service.
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $tempstore_private
   *   The tempstore.private service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    Connection $connection,
    AccountProxyInterface $current_user,
    PrivateTempStoreFactory $tempstore_private
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $connection);
    $this->currentUser = $current_user;
    $this->tempstore = $tempstore_private;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('database'),
      $container->get('current_user'),
      $container->get('tempstore.private'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function isApplicable(array $arguments = []) {
    if (!($entity_type_id = $arguments[1] ?? NULL)) {
      return FALSE;
    }

    return $this->entityTypeManager->getDefinition($entity_type_id)
      ->hasLinkTemplate('delete-multiple-form');
  }

  /**
   * Load the entities corresponding with the given identifiers.
   *
   * @param string[] $values
   *   Array of values that identify the selected entities.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *   The entities.
   */
  protected function getSelectedEntities(array $values) {
    if ($entity_type_id = $this->entityTypeId()) {
      return $this->entityTypeManager->getStorage($entity_type_id)->loadMultiple($values);
    }

    return [];
  }

  /**
   * Gets the entity-type ID from the current route.
   *
   * @return string|null
   *   The entity-type ID.
   */
  protected function entityTypeId() {
    if ($this->entityTypeId === FALSE) {
      $this->entityTypeId = $this->currentRoute->getParameter('entity_type_id') ?? NULL;
    }

    return $this->entityTypeId;
  }

  /**
   * Produces a URL query with the current page as destination.
   *
   * @return array
   *   The URL query.
   */
  protected function getUrlQuery() {
    return ['query' => ['destination' => \Drupal::request()->getRequestUri()]];
  }

}
