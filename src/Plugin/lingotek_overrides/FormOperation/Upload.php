<?php

namespace Drupal\lingotek_overrides\Plugin\lingotek_overrides\FormOperation;

use Drupal\lingotek_overrides\Plugin\lingotek_overrides\FormOperationBase;

/**
 * Defines a Lingotek plugin for the upload operation.
 *
 * @LingotekOverridesFormOperation(
 *   id = "upload",
 *   title = @Translation("Upload source for translation"),
 *   weight = 10,
 *   form_ids = {
 *     "lingotek_management",
 *     "lingotek_entity_management",
 *   },
 *   group = @Translation("Source"),
 *   options = {
 *     "job_id",
 *   }
 * )
 */
class Upload extends FormOperationBase {

}
