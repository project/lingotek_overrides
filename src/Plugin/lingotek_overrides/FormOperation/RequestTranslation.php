<?php

namespace Drupal\lingotek_overrides\Plugin\lingotek_overrides\FormOperation;

use Drupal\Core\Form\FormStateInterface;
use Drupal\lingotek_overrides\Plugin\lingotek_overrides\FormOperationBase;

/**
 * Defines a Lingotek plugin for the request_translation operation.
 *
 * @LingotekOverridesFormOperation(
 *   id = "request_translation",
 *   title = @Translation("Request translation"),
 *   weight = 30,
 *   form_ids = {
 *     "lingotek_management",
 *     "lingotek_entity_management",
 *   },
 *   group = @Translation("Translation"),
 *   options = {
 *     "language",
 *   }
 * )
 */
class RequestTranslation extends FormOperationBase {

  /**
   * {@inheritdoc}
   */
  public function callback(FormStateInterface $form_state) {
    $values = $this->getValues($form_state);
    $languages = $this->getLanguages($form_state);

    foreach ($languages as $language) {
      $callbacks[] = [
        'createLanguageRequestTranslationBatch',
        $values,
        $language,
      ];
    }

    return $callbacks ?? [];
  }

}
