<?php

namespace Drupal\lingotek_overrides\Plugin\lingotek_overrides\FormOperation;

use Drupal\lingotek_overrides\Plugin\lingotek_overrides\FormOperationBase;

/**
 * Defines a Lingotek plugin for the clear_job operation.
 *
 * @LingotekOverridesFormOperation(
 *   id = "clear_job",
 *   title = @Translation("Clear Job ID"),
 *   weight = 110,
 *   form_ids = {
 *     "lingotek_management",
 *     "lingotek_entity_management",
 *   },
 *   group = @Translation("Document")
 * )
 */
class ClearJob extends FormOperationBase {

}
