<?php

namespace Drupal\lingotek_overrides\Plugin\lingotek_overrides\FormOperation;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Defines a Lingotek plugin for the delete_translation operation.
 *
 * @LingotekOverridesFormOperation(
 *   id = "delete_content",
 *   title = @Translation("Delete content"),
 *   weight = 120,
 *   form_ids = {
 *     "lingotek_management",
 *   },
 *   group = @Translation("Document")
 * )
 */
class DeleteContent extends DeleteOperationBase {

  /**
   * {@inheritdoc}
   */
  public function callback(FormStateInterface $form_state) {
    $callbacks[] = [
      [$this, 'execute'],
      'redirectToDeleteMultipleEntitiesForm',
      $this->getValues($form_state),
      $form_state,
    ];

    return $callbacks;
  }

  /**
   * Redirects to entities' multi-delete form.
   *
   * @param array $arguments
   *   The arguments:
   *   - values: the submitted values.
   *   - form_state: the form state.
   *
   * @see \Drupal\lingotek\Form\LingotekManagementFormBase::redirectToDeleteMultipleNodesForm()
   */
  protected function redirectToDeleteMultipleEntitiesForm(array $arguments) {
    /**
     * @var array $values
     * @var \Drupal\Core\Form\FormStateInterface $form_state
     */
    [$values, $form_state] = $arguments;
    $entityInfo = [];
    $entities = $this->getSelectedEntities($values);
    foreach ($entities as $entity) {
      /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
      $language = $entity->getUntranslated()->language();
      $entityInfo[$entity->id()] = [$language->getId() => $language->getId()];
    }
    $this->tempstore->get('entity_delete_multiple_confirm')
      ->set($this->currentUser->id() . ':' . $this->entityTypeId, $entityInfo);
    $form_state->setRedirectUrl(Url::fromUserInput($entity->getEntityType()->getLinkTemplate('delete-multiple-form'), $this->getUrlQuery()));
  }

}
