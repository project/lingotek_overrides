<?php

namespace Drupal\lingotek_overrides\Plugin\lingotek_overrides\FormOperation;

use Drupal\Core\Form\FormStateInterface;
use Drupal\lingotek_overrides\Plugin\lingotek_overrides\FormOperationBase;

/**
 * Defines a Lingotek plugin for the cancel_translation operation.
 *
 * @LingotekOverridesFormOperation(
 *   id = "cancel_translation",
 *   title = @Translation("Cancel translation"),
 *   weight = 90,
 *   form_ids = {
 *     "lingotek_management",
 *     "lingotek_entity_management",
 *   },
 *   group = @Translation("Translation"),
 *   options = {
 *     "language",
 *   }
 * )
 */
class CancelTranslation extends FormOperationBase {

  /**
   * {@inheritdoc}
   */
  public function callback(FormStateInterface $form_state) {
    $values = $this->getValues($form_state);
    $languages = $this->getLanguages($form_state);

    foreach ($languages as $language) {
      $callbacks[] = [
        'createTargetCancelBatch',
        $values,
        $language,
      ];
    }

    return $callbacks ?? [];
  }

}
