<?php

namespace Drupal\lingotek_overrides\Plugin\lingotek_overrides\FormOperation;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Defines a Lingotek plugin for the delete_translation operation.
 *
 * @LingotekOverridesFormOperation(
 *   id = "delete_translation",
 *   title = @Translation("Delete translation"),
 *   weight = 60,
 *   form_ids = {
 *     "lingotek_management",
 *   },
 *   group = @Translation("Translation"),
 *   options = {
 *     "language",
 *   }
 * )
 */
class DeleteTranslation extends DeleteOperationBase {

  /**
   * {@inheritdoc}
   */
  public function callback(FormStateInterface $form_state) {
    $callbacks[] = [
      [$this, 'execute'],
      'redirectToDeleteTranslationForm',
      $this->getValues($form_state),
      $this->getLanguages($form_state),
      $form_state,
    ];

    return $callbacks;
  }

  /**
   * Redirects to translations' multi-delete form.
   *
   * @param array $arguments
   *   The arguments:
   *   - values: the submitted values.
   *   - langcodes: the submitted language codes.
   *   - form_state: the form state.
   *
   * @see \Drupal\lingotek\Form\LingotekManagementFormBase::redirectToDeleteTranslationForm()
   */
  protected function redirectToDeleteTranslationForm(array $arguments) {
    /**
     * @var array $values
     * @var array $langcodes
     * @var \Drupal\Core\Form\FormStateInterface $form_state
     */
    [$values, $langcodes, $form_state] = $arguments;
    $entityInfo = [];
    /** @var \Drupal\Core\Entity\TranslatableInterface[] $entities */
    $entities = $this->getSelectedEntities($values);
    foreach ($entities as $entity) {
      $source_langcode = $entity->getUntranslated()->language()->getId();

      foreach ($langcodes as $langcode) {
        if ($source_langcode !== $langcode && $entity->hasTranslation($langcode)) {
          /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
          $entityInfo[$entity->id()][$langcode] = $langcode;
        }
      }
    }

    if (!empty($entityInfo)) {
      $this->tempstore->get('entity_delete_multiple_confirm')
        ->set($this->currentUser->id() . ':' . $this->entityTypeId, $entityInfo);
      $form_state->setRedirectUrl(Url::fromUserInput($entity->getEntityType()->getLinkTemplate('delete-multiple-form'), $this->getUrlQuery()));
    }
    else {
      $this->messenger()->addWarning($this->t('No valid translations for deletion.'));
      // Ensure selection is persisted.
      $form_state->setRebuild();
    }
  }

}
