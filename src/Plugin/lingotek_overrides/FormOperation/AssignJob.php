<?php

namespace Drupal\lingotek_overrides\Plugin\lingotek_overrides\FormOperation;

use Drupal\lingotek_overrides\Plugin\lingotek_overrides\FormOperationBase;

/**
 * Defines a Lingotek plugin for the assign_job operation.
 *
 * @LingotekOverridesFormOperation(
 *   id = "assign_job",
 *   title = @Translation("Assign Job ID"),
 *   weight = 100,
 *   form_ids = {
 *     "lingotek_management",
 *     "lingotek_entity_management",
 *   },
 *   group = @Translation("Document")
 * )
 */
class AssignJob extends FormOperationBase {

}
