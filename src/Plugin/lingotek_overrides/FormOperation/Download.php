<?php

namespace Drupal\lingotek_overrides\Plugin\lingotek_overrides\FormOperation;

use Drupal\Core\Form\FormStateInterface;
use Drupal\lingotek_overrides\Plugin\lingotek_overrides\FormOperationBase;

/**
 * Defines a Lingotek plugin for the download operation.
 *
 * @LingotekOverridesFormOperation(
 *   id = "download",
 *   title = @Translation("Download translation"),
 *   weight = 50,
 *   form_ids = {
 *     "lingotek_management",
 *     "lingotek_entity_management",
 *   },
 *   group = @Translation("Translation"),
 *   options = {
 *     "language",
 *   }
 * )
 */
class Download extends FormOperationBase {

  /**
   * {@inheritdoc}
   */
  public function callback(FormStateInterface $form_state) {
    $values = $this->getValues($form_state);
    $languages = $this->getLanguages($form_state);

    foreach ($languages as $language) {
      $callbacks[] = [
        'createLanguageDownloadBatch',
        $values,
        $language,
      ];
    }

    return $callbacks ?? [];
  }

}
