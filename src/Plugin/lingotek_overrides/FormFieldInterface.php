<?php

namespace Drupal\lingotek_overrides\Plugin\lingotek_overrides;

use Drupal\Core\Entity\EntityInterface;

/**
 * Interface for Lingotek form-field plugins.
 */
interface FormFieldInterface extends FormPluginInterface {

  /**
   * Returns the table header for a field.
   *
   * @param string $entity_type_id
   *
   * @return mixed
   */
  public function getHeader($entity_type_id = NULL);

  /**
   * Returns the data for a field.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *
   * @return array
   */
  public function getData(EntityInterface $entity);

}
