<?php

namespace Drupal\lingotek_overrides\Plugin\lingotek_overrides\FormOption;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\lingotek_overrides\Plugin\lingotek_overrides\FormOptionBase;
use Drupal\pathauto\AliasCleanerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException;

/**
 * Defines a Lingotek plugin for the job ID option.
 *
 * @LingotekOverridesFormOption(
 *   id = "job_id",
 *   title = @Translation("Job ID"),
 *   weight = 20
 * )
 */
class JobId extends FormOptionBase {

  /**
   * The current_route_match service.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  protected $currentRoute;

  /**
   * The datetime.time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * The date.formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The pathauto.alias_cleaner service.
   *
   * @var \Drupal\pathauto\AliasCleanerInterface
   */
  protected $aliasCleaner;

  /**
   * JobId constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity_type.manager service.
   * @param \Drupal\Core\Database\Connection $connection
   *   The database service.
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $temp_store_factory
   *   The tempstore.private service.
   * @param \Drupal\Core\Routing\CurrentRouteMatch $current_route
   *   The current_route_match service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The datetime.time service.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date.formatter service.
   * @param \Drupal\pathauto\AliasCleanerInterface|null $alias_cleaner
   *   The pathauto.alias_cleaner service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    Connection $connection,
    PrivateTempStoreFactory $temp_store_factory,
    CurrentRouteMatch $current_route,
    TimeInterface $time,
    DateFormatterInterface $date_formatter,
    AliasCleanerInterface $alias_cleaner = NULL
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $connection, $temp_store_factory);
    $this->currentRoute = $current_route;
    $this->time = $time;
    $this->dateFormatter = $date_formatter;
    $this->aliasCleaner = $alias_cleaner;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    try {
      $alias_cleaner = $container->get('pathauto.alias_cleaner');
    }
    catch (ServiceNotFoundException $e) {
      $alias_cleaner = NULL;
    }

    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('database'),
      $container->get('tempstore.private'),
      $container->get('current_route_match'),
      $container->get('datetime.time'),
      $container->get('date.formatter'),
      $alias_cleaner
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildElement(array $element = []) {
    $date = $this->dateFormatter->format($this->time->getRequestTime(), 'custom', 'Y-m-d');
    $title = '';
    /** @var \Drupal\node\NodeInterface $node */
    if ($node = $this->currentRoute->getParameter('node') ?? NULL) {
      $title = $node->getUntranslated()->label();
    }

    $element = [
      '#type' => 'container',
    ];

    $default = trim("$date $title");

    $element[$this->pluginId] = [
      '#type' => 'lingotek_job_id',
      '#default_value' => $this->aliasCleaner ? $this->aliasCleaner->cleanString($default) : $default,
    ];

    $element[$this->pluginId] = parent::buildElement($element[$this->pluginId]);

    $element["{$this->pluginId}_overwrite"] = [
      '#type' => 'checkbox',
      '#description' => $this->t('If unchecked, the job ID will apply only to content without an existing one.'),
      '#title' => $this->t('Overwrite existing job ID'),
      '#default_value' => (bool) $this->tempStore->get("{$this->pluginId}_overwrite"),
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function validateElement(array &$element, FormStateInterface $form_state) {
    if ($this->allowValidation($element, $form_state)) {
      if ($option = trim($form_state->getValue($this->pluginId))) {
        if (in_array($option, ['<any>', '<none>'])) {
          $form_state->setError($element, $this->t('%option is not a valid Job ID.', ['%option' => $option]));
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getValueKeys() {
    $keys = parent::getValueKeys();
    $keys[] = "{$this->pluginId}_overwrite";
    return $keys;
  }

}
