<?php

namespace Drupal\lingotek_overrides\Plugin\lingotek_overrides\FormOption;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\lingotek_overrides\LingotekConfigurationServiceInterface;
use Drupal\lingotek_overrides\Plugin\lingotek_overrides\FormOptionBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a Lingotek plugin for the profile option.
 *
 * @LingotekOverridesFormOption(
 *   id = "profile",
 *   title = @Translation("Select profile"),
 *   weight = 30
 * )
 */
class Profile extends FormOptionBase {

  /**
   * The lingotek.configuration service.
   *
   * @var \Drupal\lingotek_overrides\LingotekConfigurationServiceInterface
   */
  protected $lingotekConfiguration;

  /**
   * Language constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity_type.manager service.
   * @param \Drupal\Core\Database\Connection $connection
   *   The database service.
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $temp_store_factory
   *   The tempstore.private service.
   * @param \Drupal\lingotek_overrides\LingotekConfigurationServiceInterface $lingotek_configuration
   *   The lingotek.configuration service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    Connection $connection,
    PrivateTempStoreFactory $temp_store_factory,
    LingotekConfigurationServiceInterface $lingotek_configuration
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $connection, $temp_store_factory);
    $this->lingotekConfiguration = $lingotek_configuration;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('database'),
      $container->get('tempstore.private'),
      $container->get('lingotek.configuration')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildElement(array $element = []) {
    return parent::buildElement([
      '#type' => 'radios',
      '#title' => $this->getTitle(),
      '#options' => $this->lingotekConfiguration->getProfileOptions(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  protected function getErrorMessage() {
    return $this->t('Select a translation profile.');
  }

}
