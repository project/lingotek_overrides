<?php

namespace Drupal\lingotek_overrides\Plugin\lingotek_overrides\FormOption;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\lingotek_overrides\Plugin\lingotek_overrides\FormOptionBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a Lingotek plugin for the language option.
 *
 * @LingotekOverridesFormOption(
 *   id = "language",
 *   title = @Translation("Select language"),
 *   weight = 10
 * )
 */
class Language extends FormOptionBase {

  /**
   * The language_manager service.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Language constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity_type.manager service.
   * @param \Drupal\Core\Database\Connection $connection
   *   The database service.
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $temp_store_factory
   *   The tempstore.private service.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language_manager service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    Connection $connection,
    PrivateTempStoreFactory $temp_store_factory,
    LanguageManagerInterface $language_manager
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $connection, $temp_store_factory);
    $this->languageManager = $language_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('database'),
      $container->get('tempstore.private'),
      $container->get('language_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildElement(array $element = []) {
    $languages = [];

    foreach ($this->languageManager->getLanguages() as $langcode => $language) {
      $languages[$langcode] = $language->getName();
    }

    return parent::buildElement([
      '#type' => 'checkboxes',
      '#title' => $this->getTitle(),
      '#options' => $languages,
    ]);
  }

  /**
   * {@inheritdoc}
   */
  protected function getErrorMessage() {
    return $this->t('Select at least one language.');
  }

}
