<?php

namespace Drupal\lingotek_overrides\Plugin\RelatedEntitiesDetector;

/**
 * @RelatedEntitiesDetector (
 *   id = "restricted_related_entities_detector",
 *   title = @Translation("Detector for restricted related entities"),
 *   weight = -10,
 * )
 */
class RestrictedRelatedEntitiesDetector extends RestrictedRelatedEntitiesDetectorBase {

  /**
   * {@inheritdoc}
   */
  protected $fieldTypes = ['entity_reference', 'entity_reference_revisions'];

}
