<?php

namespace Drupal\lingotek_overrides\Plugin\RelatedEntitiesDetector;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\lingotek\Plugin\RelatedEntitiesDetector\EntityReferenceDetectorBase;
use Drupal\lingotek_overrides\RelatedEntities\RestrictedEntitiesDetectorInterface;

/**
 * Base class for restricted-entities detector plugins.
 */
abstract class RestrictedRelatedEntitiesDetectorBase extends EntityReferenceDetectorBase implements RestrictedEntitiesDetectorInterface {

  /**
   * {@inheritdoc}
   */
  public function extract(ContentEntityInterface &$entity, array &$entities, array &$related, $depth, array $visited) {
    /** @var \Drupal\lingotek_overrides\LingotekConfigurationServiceInterface $config */
    $config = $this->lingotekConfiguration;
    // Redundant, since the calling method is already checking for restrictions.
    static $restrict;
    $restrict = is_null($restrict) ? $config->getRestrictFilter() : $restrict;
    $entity_type_id = $entity->getEntityTypeId();
    $entity_bundle = $entity->bundle();
    $entity_id = $entity->id();

    if (!in_array($entity_id, $visited[$entity_bundle] ?? [])) {
      $visited[$entity_bundle][] = $entity_id;

      if ($config->isEnabled($entity_type_id, $entity_bundle)) {
        $entities[$entity_type_id][$entity_id] = $entity->getUntranslated();
      }
    }

    if ($depth > 0) {
      --$depth;
      $field_definitions = $this->entityFieldManager->getFieldDefinitions($entity_type_id, $entity_bundle);
      // Sort fields by label.
      foreach ($field_definitions as $field_name => $field_definition) {
        $field_labels[$field_name] = $field_definition->getLabel();
      }

      asort($field_labels);
      /** @var \Drupal\Core\Field\FieldDefinitionInterface[] $field_definitions */
      $field_definitions = array_merge($field_labels, $field_definitions);

      foreach ($field_definitions as $field_name => $definition) {
        $field_type = $definition->getType();
        $field_restricted = FALSE;

        if (in_array($field_type, $this->fieldTypes)) {
          if ($restrict) {
            if (!$config->getSetting('manage_related_show', $entity_type_id, $entity_bundle, $field_name)) {
              $field_restricted = TRUE;
            }
          }

          $target_entity_type_id = $definition->getFieldStorageDefinition()
            ->getSetting('target_type');
          $target_entity_type = $this->entityTypeManager->getDefinition($target_entity_type_id);

          if ($target_entity_type instanceof ContentEntityTypeInterface) {
            $child_entities = $entity->{$field_name}->referencedEntities();
            foreach ($child_entities as $embedded_entity) {
              if ($embedded_entity !== NULL) {
                if ($embedded_entity instanceof ContentEntityInterface && $embedded_entity->isTranslatable()) {
                  if (!$restrict) {
                    if (!$config->isEnabled($embedded_entity->getEntityTypeId(), $embedded_entity->bundle())) {
                      // Out-of-box behavior.
                      continue;
                    }
                  }
                  // We need to avoid cycles if we have several entity references
                  // referencing each other.
                  $embedded_entity_bundle = $embedded_entity->bundle();
                  $embedded_entity_id = $embedded_entity->id();

                  if (!isset($visited[$embedded_entity_bundle]) || !in_array($embedded_entity_id, $visited[$embedded_entity_bundle])) {
                    if ($field_restricted) {
                      // The field's entities shouldn't be displayed, but some
                      // of their (recursive) children might have to.
                      $visited[$embedded_entity_bundle][] = $embedded_entity_id;
                    }

                    $entities = $this->extract($embedded_entity, $entities, $related, $depth, $visited);
                  }
                }
              }
            }
          }
        }
      }
    }
    return $entities;
  }

}
