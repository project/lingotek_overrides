<?php

namespace Drupal\lingotek_overrides;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\lingotek\LingotekContentTranslationServiceInterface as BaseLingotekContentTranslationServiceInterface;

/**
 * Extended interface for the content-translation service.
 */
interface LingotekContentTranslationServiceInterface extends BaseLingotekContentTranslationServiceInterface {

  /**
   * Public wrapper for ::includeMetadata().
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity.
   * @param $data
   *   The array of data.
   * @param bool $includeIntelligenceMetadata
   *   If TRUE, include intelligence metadata.
   */
  public function doIncludeMetadata(ContentEntityInterface $entity, array &$data, $includeIntelligenceMetadata = TRUE);

}
