<?php

namespace Drupal\lingotek_overrides;

/**
 * Helper trait for form-plugin managers.
 */
trait PluginManagerTrait {

  /**
   * {@inheritdoc}
   */
  public function getDefinitions() {
    // Fetch and sort definitions by weight.
    if (!($definitions = $this->getCachedDefinitions())) {
      $definitions = $this->findDefinitions();
      uasort($definitions, '\Drupal\Component\Utility\SortArray::sortByWeightElement');
      $this->setCachedDefinitions($definitions);
    }

    return $definitions;
  }

}
