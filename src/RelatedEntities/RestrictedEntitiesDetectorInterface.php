<?php

namespace Drupal\lingotek_overrides\RelatedEntities;

/**
 * Interface for plugins allowed to restrict entities.
 *
 * It exists exclusively to differentiate plugins.
 */
interface RestrictedEntitiesDetectorInterface {

}
