<?php

namespace Drupal\lingotek_overrides;

/**
 * Interface FormPluginManagerInterface.
 *
 * @package Drupal\lingotek_overrides
 */
interface FormPluginManagerInterface {

  /**
   * Gets applicable plugins for a certain form ID and entity type.
   *
   * @param $form_id
   *   The form ID.
   * @param string|null $entity_type_id
   *   The entity-type ID.
   *
   * @return \Drupal\lingotek_overrides\Plugin\lingotek_overrides\FormPluginInterface[]
   *   The form plugins.
   */
  public function getApplicable($form_id, $entity_type_id = NULL);

}
