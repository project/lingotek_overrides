<?php

namespace Drupal\lingotek_overrides;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Abstract class for all form-plugin managers.
 *
 * @package Drupal\lingotek_overrides
 */
abstract class FormPluginManagerBase extends DefaultPluginManager implements FormPluginManagerInterface {

  use PluginManagerTrait;

  /**
   * {@inheritdoc}
   */
  public function getApplicable($form_id, $entity_type_id = NULL) {
    /** @var \Drupal\lingotek_overrides\Plugin\lingotek_overrides\FormPluginInterface[] $plugins */
    $plugins = [];

    $definitions = array_filter($this->getDefinitions(), function ($definition) use ($form_id, $entity_type_id) {
      if (in_array($form_id, $definition['form_ids'])) {
        if ($entity_type_id) {
          if (!empty($definition['entity_types'])) {
            return in_array($entity_type_id, $definition['entity_types']);
          }
          // No entity types equals all entity types.
          return TRUE;
        }

        return TRUE;
      }

      return FALSE;
    });

    foreach (array_keys($definitions) as $plugin_name) {
      try {
        /** @var \Drupal\lingotek_overrides\Plugin\lingotek_overrides\FormPluginInterface $plugin */
        $plugin = $this->createInstance($plugin_name);

        if ($plugin->isApplicable([$form_id, $entity_type_id])) {
          $plugin->setEntityTypeId($entity_type_id);
          $plugins[$plugin_name] = $plugin;
        }
      }
      catch (PluginException $e) {
        // Nothing.
      }
    }

    return $plugins;
  }

}
