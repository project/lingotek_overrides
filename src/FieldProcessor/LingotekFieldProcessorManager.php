<?php

namespace Drupal\lingotek_overrides\FieldProcessor;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Provides a LingotekFieldProcessor plugin manager.
 *
 * @see \Drupal\lingotek\RelatedEntitiesDetector
 * @see plugin_api
 */
class LingotekFieldProcessorManager extends DefaultPluginManager {

  /**
   * Constructs a new LingotekFieldProcessorManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/LingotekFieldProcessor',
      $namespaces,
      $module_handler,
      'Drupal\lingotek_overrides\FieldProcessor\LingotekFieldProcessorInterface',
      'Drupal\lingotek_overrides\Annotation\LingotekFieldProcessor'
    );
    $this->alterInfo('lingotek_field_processor_info');
    $this->setCacheBackend($cache_backend, 'lingotek_field_processor_info_plugins');
  }

  /**
   * Gets the processor plugins for a given field.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The field definition.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   *
   * @return \Drupal\lingotek_overrides\FieldProcessor\LingotekFieldProcessorInterface[]
   *   The field processors.
   */
  public function getProcessorsForField(FieldDefinitionInterface $field_definition, EntityInterface $entity) {
    $field_processor_definitions = $this->getDefinitions();
    $valid_processors = [];
    uasort($field_processor_definitions, 'Drupal\Component\Utility\SortArray::sortByWeightElement');
    foreach ($field_processor_definitions as $field_processor_definition_id => $field_processor_definition) {
      /** @var \Drupal\lingotek_overrides\FieldProcessor\LingotekFieldProcessorInterface $processor */
      $processor = $this->createInstance($field_processor_definition_id, []);
      if ($processor->appliesToField($field_definition, $entity)) {
        $valid_processors[] = $processor;
      }
    }
    if (empty($valid_processors)) {
      $valid_processors[] = $this->getDefaultProcessor();
    }
    // TODO: Decide if the first applying processor should exclude others, or if we should allow to refine extractions.
    return $valid_processors;
  }

  /**
   * Gets the default processor.
   *
   * @return \Drupal\lingotek_overrides\FieldProcessor\LingotekFieldProcessorInterface
   *   The default processor.
   */
  public function getDefaultProcessor() {
    return $this->createInstance('default');
  }

}
