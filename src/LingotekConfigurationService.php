<?php

namespace Drupal\lingotek_overrides;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Field\FieldConfigInterface;
use Drupal\lingotek\LingotekConfigurationService as BaseLingotekConfigurationService;

/**
 * Overrides the lingotek.configuration service class.
 *
 * @package Drupal\lingotek_overrides
 */
class LingotekConfigurationService extends BaseLingotekConfigurationService implements LingotekConfigurationServiceInterface {

  /**
   * The lingotek.settings configuration object.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * The entity_field.manager service.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Flag for whether the restrict filter is active.
   *
   * @var bool
   */
  protected $restrictFilter;

  /**
   * LingotekConfigurationService constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config.factory service.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity_field.manager service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityFieldManagerInterface $entity_field_manager) {
    $this->config = $config_factory->getEditable('lingotek.settings');
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * Prevents serialization issues.
   *
   * @return array
   *   The properties to maintain.
   */
  public function __sleep() {
    return [];
  }

  /**
   * Restores the configuration object.
   */
  public function __wakeup() {
    // Restoring the immutable object should be enough for our purposes.
    $this->config = \Drupal::config('lingotek.settings');
  }

  /**
   * {@inheritdoc}
   */
  public function getSetting($setting, $entity_type_id, $bundle, $field_name) {
    $key = $setting . '.entity.' . $entity_type_id . '.' . $bundle . '.field.' . $field_name;
    return $this->config->get($key);
  }

  /**
   * {@inheritdoc}
   */
  public function setSetting($setting, $entity_type_id, $bundle, FieldConfigInterface $field, $value) {
    if ($field->getType() != 'entity_reference') {
      return;
    }

    $key = $setting . '.entity.' . $entity_type_id . '.' . $bundle . '.field.' . $field->getName();

    if ($value && !$this->config->get($key)) {
      $this->config->set($key, $value);
      $this->config->save();
    }
    elseif (!$value && $this->config->get($key)) {
      $this->config->clear($key);
      $this->config->save();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function setRestrictFilter($restrict = FALSE) {
    $this->restrictFilter = $restrict;
  }

  /**
   * {@inheritdoc}
   */
  public function getRestrictFilter() {
    return $this->restrictFilter;
  }

}
