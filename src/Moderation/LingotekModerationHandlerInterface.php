<?php

namespace Drupal\lingotek_overrides\Moderation;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\lingotek\Moderation\LingotekModerationHandlerInterface as BaseLingotekModerationHandlerInterface;

/**
 * Enhanced interface for the content-moderation handler.
 */
interface LingotekModerationHandlerInterface extends BaseLingotekModerationHandlerInterface {

  /**
   * Gets the latest affected revision with a given moderation state.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity.
   * @param string $state
   *   The moderation state.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface|null
   *   The revision, or NULL if none matched the given state.
   */
  public function getLatestAffectedRevisionByState(ContentEntityInterface $entity, string $state);

}
