<?php

namespace Drupal\lingotek_overrides\Moderation;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\lingotek\Moderation\LingotekContentModerationHandler as BaseLingotekContentModerationHandler;

/**
 * Enhances Drupal\lingotek\Moderation\LingotekContentModerationHandler.
 */
class LingotekContentModerationHandler extends BaseLingotekContentModerationHandler implements LingotekModerationHandlerInterface {

  /**
   * {@inheritDoc}
   */
  public function getLatestAffectedRevisionByState(ContentEntityInterface $entity, string $state = NULL) {
    // Make sure the entity is untranslated.
    $entity = $entity->getUntranslated();
    $entity_type_id = $entity->getEntityTypeId();
    /** @var \Drupal\Core\Entity\Sql\SqlContentEntityStorage $storage */
    $storage = $this->entityTypeManager->getStorage('content_moderation_state');
    $query = $storage->getQuery()
      ->accessCheck(TRUE)
      ->condition('moderation_state', $state)
      ->condition('content_entity_type_id', $entity_type_id)
      ->condition('content_entity_id', $entity->id())
      ->condition('langcode', $entity->language()->getId())
      ->sort('content_entity_revision_id', 'DESC')
      ->allRevisions();
    /** @var \Drupal\content_moderation\Entity\ContentModerationStateInterface[] $states */
    $states = $storage->loadMultipleRevisions(array_keys($query->execute()));
    $entity_revisions = [];

    foreach ($states as $state) {
      $entity_revisions[] = $state->get('content_entity_revision_id')->getString();
    }

    if ($entity_revisions) {
      $entity_type = $entity->getEntityType();
      /** @var \Drupal\Core\Entity\ContentEntityStorageInterface $entity_storage */
      $entity_storage = $this->entityTypeManager->getStorage($entity_type_id);
      /** @var \Drupal\Core\Entity\ContentEntityInterface[] $entity_revisions */
      $entity_revisions = $entity_storage->loadMultipleRevisions($entity_revisions);

      foreach ($entity_revisions as $entity_revision) {
        if ($entity_revision->get($entity_type->getKey('revision_translation_affected'))->getString()) {
          return $entity_revision;
        }
      }
    }

    return NULL;
  }

}
