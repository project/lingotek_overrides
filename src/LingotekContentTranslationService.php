<?php

namespace Drupal\lingotek_overrides;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Component\Utility\SortArray;
use Drupal\Core\Database\Connection;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\RevisionableEntityBundleInterface;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\lingotek\Exception\LingotekContentEntityStorageException;
use Drupal\lingotek\LanguageLocaleMapperInterface;
use Drupal\lingotek\Lingotek;
use Drupal\lingotek\LingotekContentTranslationService as BaseLingotekContentTranslationService;
use Drupal\lingotek\LingotekConfigTranslationServiceInterface;
use Drupal\lingotek_overrides\FieldProcessor\LingotekFieldProcessorManager;
use Drupal\lingotek_overrides\Moderation\LingotekModerationHandlerInterface;
use Drupal\lingotek_overrides\LingotekInterface;
use Drupal\node\NodeInterface;
use InvalidArgumentException;

/**
 * Decorates the lingotek.content_translation service.
 *
 * @package Drupal\lingotek_overrides
 */
class LingotekContentTranslationService extends BaseLingotekContentTranslationService implements LingotekContentTranslationServiceInterface {

  use DependencySerializationTrait;

  /**
   * The plugin.manager.lingotek_field_processor service.
   *
   * @var \Drupal\lingotek_overrides\FieldProcessor\LingotekFieldProcessorManager
   */
  protected $fieldProcessorManager;

  /**
   * Constructs a new LingotekContentTranslationService object.
   *
   * @param \Drupal\lingotek_overrides\LingotekInterface $lingotek
   *   An lingotek object.
   * @param \Drupal\lingotek\LanguageLocaleMapperInterface $language_locale_mapper
   *   The language-locale mapper.
   * @param \Drupal\lingotek\LingotekConfigurationServiceInterface $lingotek_configuration
   *   The Lingotek configuration service.
   * @param \Drupal\lingotek\LingotekConfigTranslationServiceInterface $lingotek_config_translation
   *   The Lingotek config translation service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   An entity manager object.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection object.
   * @param \Drupal\lingotek_overrides\FieldProcessor\LingotekFieldProcessorManager $field_processor_manager
   *   The plugin.manager.lingotek_field_processor service.
   */
  public function __construct(
    LingotekInterface $lingotek,
    LanguageLocaleMapperInterface $language_locale_mapper,
    LingotekConfigurationServiceInterface $lingotek_configuration,
    LingotekConfigTranslationServiceInterface $lingotek_config_translation,
    EntityTypeManagerInterface $entity_type_manager,
    LanguageManagerInterface $language_manager,
    EntityFieldManagerInterface $entity_field_manager,
    Connection $connection,
    LingotekFieldProcessorManager $field_processor_manager
  ) {
    parent::__construct($lingotek, $language_locale_mapper, $lingotek_configuration, $lingotek_config_translation, $entity_type_manager, $language_manager, $entity_field_manager, $connection);
    $this->fieldProcessorManager = $field_processor_manager;
  }

  /**
   * Magic getter.
   *
   * @param string $key
   *   The property name.
   *
   * @return mixed
   *   The property value.
   *
   */
  public function __get(string $key) {
    if (property_exists($this, $key)) {
      return $this->{$key};
    }

    throw new InvalidArgumentException("The requested property '$key' does not exist.");
  }

  /**
   * {@inheritdoc}
   */
  public function getSourceData(ContentEntityInterface &$entity, &$visited = []) {
    // Logic adapted from Content Translation core module and TMGMT contrib
    // module for pulling translatable field info from content entities.
    $source_entity = NULL;

    if ($entity instanceof RevisionableInterface) {
      /** @var \Drupal\lingotek\Moderation\LingotekModerationFactoryInterface $moderation_factory */
      $moderation_factory = \Drupal::service('lingotek.moderation_factory');
      $moderation_handler = $moderation_factory->getModerationHandler();
      // Check that handler was overridden in lingotek_overrides.
      if ($moderation_handler instanceof LingotekModerationHandlerInterface && $moderation_handler->isModerationEnabled($entity)) {
        // Moderation is enabled.
        if ($upload_status = $moderation_factory->getModerationConfigurationService()->getUploadStatus($entity->getEntityTypeId(), $entity->bundle())) {
          // Get latest revision with the specific moderation state for the language.
          $source_entity = $moderation_handler->getLatestAffectedRevisionByState($entity, $upload_status);
        }
      }
    }

    $source_entity = $source_entity ?? $entity->getUntranslated();

    $isParentEntity = count($visited) === 0;
    $visited[$entity->bundle()][] = $entity->id();
    $entity_type = $entity->getEntityType();
    $field_definitions = $this->entityFieldManager->getFieldDefinitions($entity->getEntityTypeId(), $entity->bundle());
    $translatable_fields = [];
    // We need to include computed fields, as we may have a URL alias.
    foreach ($entity->getFields(TRUE) as $field_name => $definition) {
      if ($this->lingotekConfiguration->isFieldLingotekEnabled($entity->getEntityTypeId(), $entity->bundle(), $field_name)
        && $field_name != $entity_type->getKey('langcode')
        && $field_name != $entity_type->getKey('default_langcode')) {
        $translatable_fields[$field_name] = $definition;
      }
    }
    $default_display = $this->entityTypeManager->getStorage('entity_view_display')
      ->load($entity_type->id() . '.' . $entity->bundle() . '.' . 'default');
    if ($default_display !== NULL) {
      uksort($translatable_fields, function ($a, $b) use ($default_display) {
        $default = ['weight' => 99999];
        return SortArray::sortByWeightElement($default_display->getComponent($a) ?? $default, $default_display->getComponent($b) ?? $default);
      });
    }

    $data = [];
    foreach ($translatable_fields as $field_name => $definition) {
      // Start lingotek_overrides code.
      $field_processors = $this->fieldProcessorManager->getProcessorsForField($field_definitions[$field_name], $source_entity);

      foreach ($field_processors as $field_processor) {
        $field_processor->extract($source_entity, $field_name, $field_definitions[$field_name], $data, $visited);
      }
      // End lingotek_overrides code.
    }
    // Embed entity metadata. We need to exclude intelligence metadata if it is
    // a child entity.
    $this->includeMetadata($source_entity, $data, $isParentEntity);
    return $data;
  }

  /**
   * Gets an entity's translatable fields.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity.
   *
   * @return array
   *   The translatable fields.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getTranslatableFields(ContentEntityInterface $entity) {
    $entity_type = $entity->getEntityType();
    $entity_type_id = $entity->getEntityTypeId();
    $bundle = $entity->bundle();
    $translatable_fields = [];
    // We need to include computed fields, as we may have a URL alias.
    foreach ($entity->getFields(TRUE) as $field_name => $definition) {
      if ($this->lingotekConfiguration->isFieldLingotekEnabled($entity_type_id, $bundle, $field_name)
        && $field_name != $entity_type->getKey('langcode')
        && $field_name != $entity_type->getKey('default_langcode')) {
        $translatable_fields[$field_name] = $definition;
      }
    }
    $default_display = $this->entityTypeManager->getStorage('entity_view_display')
      ->load($entity_type_id . '.' . $bundle . '.' . 'default');
    if ($default_display !== NULL) {
      uksort($translatable_fields, function ($a, $b) use ($default_display) {
        return SortArray::sortByKeyString($default_display->getComponent($a), $default_display->getComponent($b), 'weight');
      });
    }

    return $translatable_fields;
  }

  /**
   * {@inheritdoc}
   */
  public function saveTargetData(ContentEntityInterface &$entity, $langcode, $data) {
    // Without a defined langcode, we can't proceed
    if (!$langcode) {
      // TODO: log warning that downloaded translation's langcode is not enabled.
      return FALSE;
    }

    try {
      // We need to load the revision that was uploaded for consistency. For that,
      // we check if we have a valid revision in the response, and if not, we
      // check the date of the uploaded document.

      /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
      $revision = (isset($data['_lingotek_metadata']) && isset($data['_lingotek_metadata']['_entity_revision'])) ? $data['_lingotek_metadata']['_entity_revision'] : NULL;
      $revision = $this->loadUploadedRevision($entity, $revision);

      // We should reload the last revision of the entity at all times.
      // This check here is only because of the case when we have asymmetric
      // paragraphs for translations, as in that case we get a duplicate that
      // still has not a valid entity id.
      // Also take into account that we may have just removed paragraph
      // translations form previous translation approaches, and in that case we
      // are forced to remove those, but there will be a mark of translation
      // changes.
      if ($entity->id() && !$entity->hasTranslationChanges()) {
        $entity = $this->entityTypeManager->getStorage($entity->getEntityTypeId())->load($entity->id());
      }

      // Initialize the translation on the Drupal side, if necessary.
      /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
      if (!$entity->hasTranslation($langcode)) {
        $entity->addTranslation($langcode, $revision->toArray());
      }
      $translation = $entity->getTranslation($langcode);

      foreach ($data as $name => $field_data) {
        if (strpos($name, '_') === 0) {
          // Skip special fields underscored.
          break;
        }
        $field_definition = $entity->getFieldDefinition($name);
        if ($field_definition && ($field_definition->isTranslatable() || $field_definition->getType() === 'cohesion_entity_reference_revisions' || $field_definition->getType() === 'entity_reference_revisions')
          && $this->lingotekConfiguration->isFieldLingotekEnabled($entity->getEntityTypeId(), $entity->bundle(), $name)) {
          // Start lingotek_overrides code.
          $field_processors = $this->fieldProcessorManager->getProcessorsForField($field_definition, $revision);

          if (count($field_processors) > 0) {
            $field_processors = array_reverse($field_processors);
            $field_processor = reset($field_processors);
            $field_processor->store($translation, $langcode, $revision, $name, $field_definition, $data[$name]);
          }
          // End lingotek_overrides code.
        }
      }

      // We need to set the content_translation source so the files are synced
      // properly. See https://www.drupal.org/node/2544696 for more information.
      $translation->set('content_translation_source', $entity->getUntranslated()->language()->getId());

      $entity->lingotek_processed = TRUE;
      // Allow other modules to alter the translation before is saved.
      \Drupal::moduleHandler()->invokeAll('lingotek_content_entity_translation_presave', [&$translation, $langcode, $data]);

      $published_field = $entity->getEntityType()->getKey('published');
      $published_field_definition = $entity->getFieldDefinition($published_field);
      if ($published_field_definition !== NULL && $published_field_definition->isTranslatable()) {
        $published_setting = $this->lingotekConfiguration->getPreference('target_download_status');
        if ($published_setting !== "same-as-source") {
          $published_value = ($published_setting === 'published') ? NodeInterface::PUBLISHED : NodeInterface::NOT_PUBLISHED;
          $translation->set($published_field, $published_value);
        }
      }

      // If there is any content moderation module is enabled, we may need to
      // perform a transition in their workflow.
      /** @var \Drupal\lingotek\Moderation\LingotekModerationFactoryInterface $moderation_factory */
      $moderation_factory = \Drupal::service('lingotek.moderation_factory');
      $moderation_handler = $moderation_factory->getModerationHandler();
      $moderation_handler->performModerationTransitionIfNeeded($translation);

      if ($moderation_handler->isModerationEnabled($translation) &&
          $translation->getEntityType()->isRevisionable()) {
        if ($bundle_entity_type = $entity->getEntityType()->getBundleEntityType()) {
          $bundle_entity = $this->entityTypeManager->getStorage($bundle_entity_type)->load($entity->bundle());
          if ($bundle_entity instanceof RevisionableEntityBundleInterface) {
            $translation->setNewRevision($bundle_entity->shouldCreateNewRevision());
          }
        }
        if ($translation instanceof RevisionLogInterface && $translation->isNewRevision()) {
          $requestTime = \Drupal::time()->getRequestTime();
          $translation->setRevisionUserId(\Drupal::currentUser()->id());
          $translation->setRevisionCreationTime($requestTime);
          $translation->setRevisionLogMessage((string) new FormattableMarkup('Document translated into @langcode by Lingotek.', ['@langcode' => strtoupper($langcode)]));
        }

        $entity->set($entity->getEntityType()->getKey('revision_translation_affected'), '');
      }

      $translation->save();

      return $entity;
    }
    catch (EntityStorageException $storage_exception) {
      $this->setTargetStatus($entity, $langcode, Lingotek::STATUS_ERROR);
      throw new LingotekContentEntityStorageException($entity, $storage_exception, $storage_exception->getMessage());
    }
  }

  /**
   * {@inheritDoc}
   */
  public function doIncludeMetadata(ContentEntityInterface $entity, array &$data, $includeIntelligenceMetadata = TRUE) {
    $this->includeMetadata($entity, $data, $includeIntelligenceMetadata);
  }

}
