<?php

namespace Drupal\lingotek_overrides\Annotation;

/**
 * Defines a LingotekOverridesFormOption annotation object.
 *
 * @Annotation
 */
class LingotekOverridesFormOption extends LingotekOverridesFormPluginBase {

}
