<?php

namespace Drupal\lingotek_overrides\Annotation;

/**
 * Defines a LingotekOverridesFormOperation annotation object.
 *
 * @Annotation
 */
class LingotekOverridesFormOperation extends LingotekOverridesFormPluginBase {

  /**
   * The plugin IDs of available options.
   *
   * @var array
   */
  public $options = [];

}
