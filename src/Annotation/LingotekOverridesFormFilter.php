<?php

namespace Drupal\lingotek_overrides\Annotation;

/**
 * Defines a LingotekOverridesFormFilter annotation object.
 *
 * @Annotation
 */
class LingotekOverridesFormFilter extends LingotekOverridesFormPluginBase {

}
