<?php

namespace Drupal\lingotek_overrides\Annotation;

/**
 * Defines a LingotekOverridesFormField annotation object.
 *
 * @Annotation
 */
class LingotekOverridesFormField extends LingotekOverridesFormPluginBase {

}
