<?php

namespace Drupal\lingotek_overrides;

use Drupal\Core\Field\FieldConfigInterface;
use Drupal\lingotek\LingotekConfigurationServiceInterface as BaseLingotekConfigurationServiceInterface;

/**
 * Interface LingotekConfigurationServiceInterface.
 *
 * @package Drupal\lingotek_overrides
 */
interface LingotekConfigurationServiceInterface extends BaseLingotekConfigurationServiceInterface {

  /**
   * Gets a field-configuration setting.
   *
   * @param $setting
   *   The setting name.
   * @param $entity_type_id
   *   The entity-type ID.
   * @param $bundle
   *   The bundle name.
   * @param $field_name
   *   The field name.
   *
   * @return mixed
   *   The setting value.
   */
  public function getSetting($setting, $entity_type_id, $bundle, $field_name);

  /**
   *
   *
   * @param $setting
   *   The setting name.
   * @param $entity_type_id
   *   The entity-type ID.
   * @param $bundle
   *   The bundle name.
   * @param \Drupal\Core\Field\FieldConfigInterface $field
   *   The field configuration.
   * @param mixed $value
   *   The setting value.
   */
  public function setSetting($setting, $entity_type_id, $bundle, FieldConfigInterface $field, $value);

  /**
   * Sets the $restrictFilter property.
   *
   * @param bool $restrict
   *   The value to set.
   */
  public function setRestrictFilter($restrict = FALSE);

  /**
   * Gets the value of the $restrictFilter property.
   *
   * @return bool
   *   If TRUE, the restrict filter is active.
   */
  public function getRestrictFilter();

}
