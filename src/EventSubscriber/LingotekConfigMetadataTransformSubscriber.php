<?php

namespace Drupal\lingotek_overrides\EventSubscriber;

use Drupal\Core\Config\ConfigEvents;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Config\StorageTransformEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\StorageInterface;

/**
 * Handles the Lingotek metadata configuration.
 *
 * Lingotek automatically generates metadata configuration entities when you
 * make a change to another config entity, when you do an export it can create
 * 2x the amount of configuration in your code base. These are mostly
 * unnecessary.
 *
 * This adds in some dynamic logic to handle this configuration so that:
 * 1. It doesn't flood your sync directory.
 * 2. It stays in sync with your configuration entities.
 *
 * @package Drupal\lingotek_overrides\EventSubscriber
 */
class LingotekConfigMetadataTransformSubscriber implements EventSubscriberInterface {

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The active config storage.
   *
   * @var \Drupal\Core\Config\StorageInterface
   */
  protected $activeStorage;

  /**
   * The sync config storage.
   *
   * @var \Drupal\Core\Config\StorageInterface
   */
  protected $syncStorage;

  /**
   * Constructs a new event subscriber instance.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Config\StorageInterface $config_storage
   *   The config active storage.
   * @param \Drupal\Core\Config\StorageInterface $sync_storage
   *   The sync config storage.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager, StorageInterface $config_storage, StorageInterface $sync_storage) {
    $this->configFactory = $config_factory;
    $this->entityTypeManager = $entity_type_manager;
    $this->activeStorage = $config_storage;
    $this->syncStorage = $sync_storage;
  }

  /**
   * Determine whether this is enabled or not.
   *
   * @return boolean
   *   TRUE if enabled, FALSE if not.
   */
  protected function isEnabled() {
    return $this->configFactory->get('lingotek_overrides.config_settings')->get('disable_config_metadata_export');
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[ConfigEvents::STORAGE_TRANSFORM_IMPORT][] = ['onImportTransform'];
    $events[ConfigEvents::STORAGE_TRANSFORM_EXPORT][] = ['onExportTransform'];
    return $events;
  }

  /**
   * The storage is transformed for exporting.
   *
   * @param \Drupal\Core\Config\StorageTransformEvent $event
   *   The config storage transform event.
   */
  public function onExportTransform(StorageTransformEvent $event) {
    if ($this->isEnabled()) {
      /** @var \Drupal\Core\Config\StorageInterface $storage */
      $storage = $event->getStorage();
      // Never export lingotek metadata config entities.
      $list = $storage->listAll();
      foreach ($list as $config_name) {
        if ($this->matchConfigName($config_name) > 0) {
          $exists = $this->syncStorage->exists($config_name);
          // If it doesn't exist in sync directory then do not export it.
          if (!$exists) {
            $storage->delete($config_name);
          }
        }
      }
    }
  }

  /**
   * The storage is transformed for importing.
   *
   * @param \Drupal\Core\Config\StorageTransformEvent $event
   *   The config storage transform event.
   */
  public function onImportTransform(StorageTransformEvent $event) {
    if ($this->isEnabled()) {
      /** @var \Drupal\Core\Config\StorageInterface $storage */
      $storage = $event->getStorage();
      $list = $this->activeStorage->listAll();
      foreach ($list as $config_name) {
        if ($this->matchConfigName($config_name) > 0) {
          // Parse the name of the lingotek metadata config entity. If this
          // lingotek metadata config entity depends on any other configuration
          // then see if its stored config or active config.
          // - If it's stored config (exported) then ignore the metadata.
          // - Otherwise allow the metadata to be deleted.
          $config = str_replace('lingotek.lingotek_config_metadata.', '', $config_name);
          list($dependent_type, $dependent_name) = explode('.', $config);

          // Find the config that depends on the lingotek metadata.
          // (e.g a view, content type etc.)
          $definition = $this->entityTypeManager->getDefinition($dependent_type);
          $dependent = $definition->getConfigPrefix() . '.' . $dependent_name;
          $exists = $this->syncStorage->exists($dependent);
          // If dependent exists in sync directory then ignore lingotek metadata.
          if ($exists) {
            $data = $this->activeStorage->read($config_name);
            if (!empty($data)) {
              $storage->write($config_name, $data);
            }
          }
        }
      }
    }
  }

  /**
   * Match the lingotek config metadata name.
   *
   * @param string $name
   *   The config metadata name.
   *
   * @return bool
   *   True or False.
   */
  protected function matchConfigName($name) {
    if (substr_count($name, 'lingotek.lingotek_config_metadata') > 0) {
      return TRUE;
    }

    return FALSE;
  }

}
