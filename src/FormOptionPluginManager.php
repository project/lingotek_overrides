<?php

namespace Drupal\lingotek_overrides;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Manages Lingotek form-option plugins.
 *
 * @see \Drupal\lingotek_overrides\Plugin\lingotek_overrides\FormOptionInterface
 * @see \Drupal\lingotek_overrides\Plugin\lingotek_overrides\FormOptionBase
 * @see \Drupal\lingotek_overrides\Annotation\LingotekOverridesFormOption
 * @see \hook_lingotek_overrides_form_option_alter()
 */
class FormOptionPluginManager extends DefaultPluginManager implements FormOptionPluginManagerInterface {

  use PluginManagerTrait;

  /**
   * FormOptionPluginManager constructor.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   The cache.discovery service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module_handler service.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/lingotek_overrides/FormOption', $namespaces, $module_handler, 'Drupal\lingotek_overrides\Plugin\lingotek_overrides\FormOptionInterface', 'Drupal\lingotek_overrides\Annotation\LingotekOverridesFormOption');

    $this->alterInfo('lingotek_overrides_form_option');
    $this->setCacheBackend($cache_backend, 'lingotek_overrides_form_option');
  }

  /**
   * {@inheritdoc}
   */
  public function getApplicable(array $operations = [], string $entity_type_id = NULL) {
    $options = [];
    /** @var \Drupal\lingotek_overrides\Plugin\lingotek_overrides\FormOperationInterface[] $operations */
    foreach ($operations as $operation_id => $operation) {
      foreach ($operation->getOptions() as $option) {
        $options[$option][$operation_id] = $operation;
      }
    }

    $definitions = array_intersect_key($this->getDefinitions(), $options);
    /** @var \Drupal\lingotek_overrides\Plugin\lingotek_overrides\FormOptionInterface[] $plugins */
    $plugins = [];

    foreach (array_keys($definitions) as $plugin_name) {
      try {
        /** @var \Drupal\lingotek_overrides\Plugin\lingotek_overrides\FormOptionInterface $plugin */
        $plugin = $this->createInstance($plugin_name);

        if ($plugin->isApplicable([$operations, $entity_type_id])) {
          $plugin->setOperations($options[$plugin_name])->setEntityTypeId($entity_type_id);
          $plugins[$plugin_name] = $plugin;
        }
      }
      catch (PluginException $e) {
        // Nothing.
      }
    }

    return $plugins;
  }

}
