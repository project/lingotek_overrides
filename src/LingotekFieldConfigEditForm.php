<?php

namespace Drupal\lingotek_overrides;

use Drupal\Core\Form\FormStateInterface;
use Drupal\lingotek\Form\LingotekFieldConfigEditForm as BaseLingotekFieldConfigEditForm;

/**
 * Overrides the lingotek.field_config_edit_form service class.
 *
 * @package Drupal\lingotek_overrides
 */
class LingotekFieldConfigEditForm extends BaseLingotekFieldConfigEditForm {

  /**
   * The lingotek.configuration service.
   *
   * @var \Drupal\lingotek_overrides\LingotekConfigurationServiceInterface
   */
  protected $lingotekConfig;

  /**
   * {@inheritdoc}
   */
  public function form(array &$form, FormStateInterface $form_state) {
    // Prevent error in the parent due to a missing existence check.
    $form['translatable']['#disabled'] = $form['translatable']['#disabled'] ?? FALSE;
    parent::form($form, $form_state);
    /** @var \Drupal\field\FieldConfigInterface $field */
    $field = $form_state->getFormObject()->getEntity();
    if ($field->getType() == 'entity_reference') {
      $entity_id = $field->getTargetEntityTypeId();
      $bundle_id = $field->getTargetBundle();

      $form['manage_related_show'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Always show related entities'),
        '#description' => $this->t('If checked, related entities will always be shown in an entity’s Lingotek-management page (node/{nid}/manage).'),
        '#default_value' => $this->lingotekConfig->getSetting('manage_related_show', $entity_id, $bundle_id, $field->getName()),
      ];

      $form['actions']['submit']['#submit'][] = [$this, 'submitForm'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\Core\Field\FieldConfigInterface $field */
    $field = $form_state->getFormObject()->getEntity();
    $entity_id = $field->getTargetEntityTypeId();
    $bundle_id = $field->getTargetBundle();
    $this->lingotekConfig->setSetting('manage_related_show', $entity_id, $bundle_id, $field, $form_state->getValue('manage_related_show'));

    parent::submitForm($form, $form_state);
  }

}
