<?php

namespace Drupal\lingotek_overrides\Remote;

use Drupal\lingotek\Remote\LingotekApiInterface as BaseLingotekApiInterface;

/**
 * Extended Lingotek Api interface.
 *
 * @package Drupal\lingotek_overrides\Remote
 */
interface LingotekApiInterface extends BaseLingotekApiInterface {

  /**
   * Makes a GET call to the /task/{id}/content endpoint.
   *
   * @param $id
   *   The task ID.
   *
   * @return mixed
   *   The response.
   *
   * @throws \Drupal\lingotek\Exception\LingotekApiException
   */
  public function getTaskContent($id);

  /**
   * Makes a GET call to the /document/{id}/content endpoint.
   *
   * @param $doc_id
   *   The document ID.
   *
   * @return mixed
   *   The document content.
   *
   * @throws \Drupal\lingotek\Exception\LingotekApiException
   */
  public function getDocumentFile($doc_id);

  /**
   * Makes a PATCH call to the /document/{id}/content endpoint.
   *
   * @param $id
   *   The task ID.
   * @param $args
   *   The arguments.
   *
   * @return mixed
   *   The response.
   * @throws \Drupal\lingotek\Exception\LingotekApiException
   */
  public function patchTaskContent($id, $args);

}
