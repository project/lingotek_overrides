<?php

namespace Drupal\lingotek_overrides\Remote;

use Drupal\lingotek\Exception\LingotekApiException;
use Drupal\lingotek\Remote\LingotekApi as BaseLingotekApi;

/**
 * Decorates the lingotek.api service.
 *
 * @package Drupal\lingotek_overrides\Remote
 */
class LingotekApi extends BaseLingotekApi implements LingotekApiInterface {

  /**
   * {@inheritdoc}
   */
  public function getDocumentFile($doc_id) {
    try {
      $this->logger->debug('Lingotek::getDocumentContent called with id ' . $doc_id);
      $response = $this->lingotekClient->get('/api/document/' . $doc_id . '/content', ['single_file' => 'text/csv', 'include_ignored' => FALSE]);
    }
    catch (\Exception $e) {
      throw new LingotekApiException('Failed to get document: ' . $e->getMessage());
    }
    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function getTaskContent($id) {
    try {
      $this->logger->debug('LingotekApi::getTaskContent called with id ' . $id);
      $response = $this->lingotekClient->get("/api/task/$id/content");
    }
    catch (\Exception $e) {
      $this->logger->error('Error getting task: %message.', ['%message' => $e->getMessage()]);
      throw new LingotekApiException('Failed to get document status: ' . $e->getMessage());
    }
    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function patchTaskContent($id, $args) {
    try {
      $this->logger->debug('Lingotek::patchTaskContent called with id ' . $id);
      $response = $this->lingotekClient->patch("/api/task/$id/content", $args, TRUE);
    }
    catch (\Exception $e) {
      throw new LingotekApiException('Failed to patch task: ' . $e->getMessage());
    }
    return $response;
  }

}
