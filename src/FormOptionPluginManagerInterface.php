<?php

namespace Drupal\lingotek_overrides;

/**
 * Interface FormOptionPluginManagerInterface.
 *
 * @package Drupal\lingotek_overrides
 */
interface FormOptionPluginManagerInterface {

  /**
   * Gets applicable plugins for a the given operations.
   *
   * @param \Drupal\lingotek_overrides\Plugin\lingotek_overrides\FormOperationInterface[] $operations
   *   The operation plugins.
   * @param string $entity_type_id
   *   The entity-type ID, if any.
   *
   * @return \Drupal\lingotek_overrides\Plugin\lingotek_overrides\FormOptionInterface[]
   *   The applicable option plugins.
   */
  public function getApplicable(array $operations = [], string $entity_type_id = NULL);

}
