<?php

namespace Drupal\lingotek_overrides\Entity;

use Drupal\lingotek\Entity\LingotekContentMetadata;

/**
 * Makes lingotek_content_metadata entities play better with TMGMT.
 *
 * @package Drupal\lingotek_overrides\Entity
 */
class LingotekOverridesContentMetadata extends LingotekContentMetadata {

  /**
   * {@inheritdoc}
   */
  public function getTranslation($langcode) {
    // This entity type is not actually translatable: pretend it is by returning
    // the entity itself instead of an actual translation.
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    if ($entity = $this->entityTypeManager()->getStorage($this->getContentEntityTypeId())->load($this->getContentEntityId())) {
      return $entity->label();
    }

    return '';
  }

}
