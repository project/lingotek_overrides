<?php

namespace Drupal\lingotek_overrides;

use Drupal\lingotek\LingotekInterface as BaseLingotekInterface;

/**
 * Interface for the extended Lingotek service.
 *
 * @package Drupal\lingotek_overrides
 */
interface LingotekInterface extends BaseLingotekInterface {

  /**
   * Gets the Lingotek API object.
   *
   * @return \Drupal\lingotek_overrides\Remote\LingotekApiInterface
   *   The API.
   */
  public function getApi();

  /**
   * Makes a GET call to the /document/{id}/translation endpoint.
   *
   * @param mixed $id
   *   The document ID.
   *
   * @return mixed
   */
  public function getTranslation($id);

  /**
   * Makes a GET call to the /document/{id}/content endpoint.
   *
   * @param mixed $id
   *   The document ID.
   *
   * @return mixed
   */
  public function getFile($id);

  /**
   * Gets the phases of a translation.
   *
   * @param mixed $id
   *   The task ID.
   *
   * @return mixed
   */
  public function getPhases($id);

  /**
   * Makes a GET call to the /task/{id}/content endpoint.
   *
   * @param mixed $id
   *   The task ID.
   *
   * @return mixed
   *   The task content.
   */
  public function getTaskContent($id);

  /**
   * Makes a PATCH call to the /task/{id}/content endpoint.
   *
   * @param mixed $id
   *   The task ID.
   * @param mixed $data
   *   The data.
   *
   * @return mixed
   */
  public function patchTaskContent($id, $data);

}
