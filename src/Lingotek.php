<?php

namespace Drupal\lingotek_overrides;

use Drupal\Component\Serialization\SerializationInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\lingotek\Exception\LingotekApiException;
use Drupal\lingotek\LanguageLocaleMapperInterface;
use Drupal\lingotek\Lingotek as BaseLingotek;
use Drupal\lingotek\LingotekFilterManagerInterface;
use Drupal\lingotek_overrides\Remote\LingotekApiInterface;
use GuzzleHttp\Psr7\Response as GuzzleResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Decorates the lingotek service.
 *
 * @package Drupal\lingotek_overrides
 */
class Lingotek extends BaseLingotek implements LingotekInterface {

  /**
   * The Lingotek API.
   *
   * @var \Drupal\lingotek_overrides\Remote\LingotekApiInterface
   */
  protected $api;

  /**
   * The serialization.json service.
   *
   * @var \Drupal\Component\Serialization\SerializationInterface
   */
  protected $json;

  /**
   * The logger channel for lingotek_overrides.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Translations phases, keyed by document ID.
   *
   * @var array
   */
  protected $documentPhases = [];

  /**
   * Lingotek constructor.
   *
   * @param \Drupal\lingotek_overrides\Remote\LingotekApiInterface $api
   *   The lingotek service.
   * @param \Drupal\lingotek\LanguageLocaleMapperInterface $language_locale_mapper
   *   The language-locale mapper.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\lingotek\LingotekFilterManagerInterface $lingotek_filter_manager
   *   The Lingotek Filter manager.
   * @param LingotekConfigurationServiceInterface $lingotek_configuration
   *   The lingotek.configuration service.
   * @param \Drupal\Component\Serialization\SerializationInterface $json
   *   The serialization.json service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   The logger.
   */
  public function __construct(LingotekApiInterface $api, LanguageLocaleMapperInterface $language_locale_mapper, ConfigFactoryInterface $config_factory, LingotekFilterManagerInterface $lingotek_filter_manager, LingotekConfigurationServiceInterface $lingotek_configuration = NULL, SerializationInterface $json, LoggerChannelFactoryInterface $logger) {
    parent::__construct($api, $language_locale_mapper, $config_factory, $lingotek_filter_manager, $lingotek_configuration);
    $this->json = $json;
    $this->logger = $logger->get('lingotek_overrides');
  }

  /**
   * Prevents serializing properties to avoid closure serialization.
   *
   * @return array
   *   The properties to maintain.
   */
  public function __sleep() {
    return [];
  }

  /**
   * Restores injected dependencies.
   */
  public function __wakeup() {
    $container = \Drupal::getContainer();
    $this->api = $container->get('lingotek.api');
    $this->languageLocaleMapper = $container->get('lingotek.language_locale_mapper');
    $this->configFactory = $container->get('config.factory');
    $this->lingotekFilterManager = $container->get('lingotek.filter_manager');
    $this->lingotekConfiguration = $container->get('lingotek.configuration');
    $this->json = $container->get('serialization.json');
    $this->logger = $container->get('logger.factory')->get('lingotek_overrides');
  }

  /**
   * {@inheritdoc}
   */
  public function getApi() {
    return $this->api;
  }

  /**
   * {@inheritdoc}
   */
  public function getTranslation($id) {
    $response = $this->api->getDocumentTranslationStatuses($id);
    return $this->handleResponse($response);
  }

  /**
   * {@inheritdoc}
   */
  public function getPhases($id) {
    if (empty($this->documentPhases[$id])) {
      $this->documentPhases[$id] = [];

      if ($translation = $this->getTranslation($id)) {
        foreach ($translation['entities'] ?? [] as $item) {
          $phases = [];
          $locale = '';

          foreach ($item['entities'] as $properties) {
            if ($type = $properties['class'][0] ?? NULL) {

              switch ($type) {
                case 'phases':
                  foreach ($properties['entities'] as $phase) {
                    $phases[$phase['properties']['id']] = $phase['properties'];
                  }
                  break;

                case 'locale':
                  $locale = strtolower($properties['properties']['code']);
                  break;
              }
            }
          }

          if ($phases && $locale) {
            $this->documentPhases[$id][$locale] = $phases;
          }
        }
      }
    }

    return $this->documentPhases[$id];
  }

  /**
   * {@inheritdoc}
   */
  public function getTaskContent($id) {
    try {
      $response = $this->api->getTaskContent($id);
      return $this->handleResponse($response, FALSE);
    }
    catch (LingotekApiException $e) {
      $this->logger->error($e->getMessage());
    }

    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getFile($id) {
    try {
      $response = $this->api->getDocumentFile($id);
      return $this->handleResponse($response);
    }
    catch (LingotekApiException $e) {
      $this->logger->error($e->getMessage());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function patchTaskContent($id, $data) {
    try {
      $response = $this->api->patchTaskContent($id, ['content' => $data]);
      return $this->handleResponse($response);
    }
    catch (LingotekApiException $e) {
      $this->logger->error($e->getMessage());
    }
  }

  /**
   * Returns data from an HTTP response.
   *
   * @param \GuzzleHttp\Psr7\Response $response
   *   The response.
   * @param bool $json
   *   Whether the JSON content should be decoded.
   *
   * @return bool|mixed|string
   *   The data.
   */
  protected function handleResponse(GuzzleResponse $response, $json = TRUE) {
    $statusCode = $response->getStatusCode();
    if ($statusCode == Response::HTTP_OK) {
      if ($json) {
        return $this->json::decode($response->getBody());
      }

      return $response->getBody()->getContents();
    }
    elseif ($statusCode == Response::HTTP_GONE) {
      // Set the status of the document back to its pre-uploaded state.
      // Typically this means the state would be set to Upload, or None but this
      // may vary depending on connector. Essentially, the content’s status
      // indicator should show that the source content needs to be re-uploaded
      // to Lingotek.
      return FALSE;
    }
    elseif ($statusCode == Response::HTTP_ACCEPTED) {
      return TRUE;
    }

    return FALSE;
  }

}
