<?php

namespace Drupal\lingotek_overrides\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides a UI for handling Lingotek config export settings.
 */
class ConfigSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'lingotek_overrides.config_settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'lingotek_overrides_config_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, Request $request = NULL) {
    $settings = $this->config('lingotek_overrides.config_settings');

    $form['disable_config_metadata_export'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Do not export Lingotek Config Metadata'),
      '#description' => $this->t('Lingotek generates Config Metadata entities whenever you make a change to other config entities. Enable this to properly disable exporting Lingotek metadata.'),
      '#default_value' => $settings->get('disable_config_metadata_export'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $config_ignore_settings = $this->config('lingotek_overrides.config_settings');
    $config_ignore_settings->set('disable_config_metadata_export', $values['disable_config_metadata_export']);
    $config_ignore_settings->save();
    parent::submitForm($form, $form_state);
  }

}
