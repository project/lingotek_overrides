<?php

namespace Drupal\lingotek_overrides\Form;

use Drupal\content_translation\ContentTranslationManagerInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Query\PagerSelectExtender;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\Core\Utility\LinkGeneratorInterface;
use Drupal\lingotek\Form\LingotekManagementForm as BaseLingotekManagementForm;
use Drupal\lingotek\LanguageLocaleMapperInterface;
use Drupal\lingotek\Lingotek;
use Drupal\lingotek\LingotekConfigurationServiceInterface;
use Drupal\lingotek\LingotekContentTranslationServiceInterface;
use Drupal\lingotek\LingotekInterface;
use Drupal\lingotek_overrides\FormFieldPluginManager;
use Drupal\lingotek_overrides\FormFilterPluginManager;
use Drupal\lingotek_overrides\FormOperationPluginManager;
use Drupal\lingotek_overrides\FormOptionPluginManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Overrides Lingotek's bulk content-management form.
 */
class LingotekManagementForm extends BaseLingotekManagementForm {

  use LingotekManagementFormTrait;

  /**
   * Whether the entity type has bundles.
   *
   * @var bool|null
   */
  protected $hasBundles = NULL;

  /**
   * LingotekManagementForm constructor.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The current database connection.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity manager.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\lingotek\LingotekInterface $lingotek
   *   The Lingotek service.
   * @param \Drupal\lingotek\LingotekConfigurationServiceInterface $lingotek_configuration
   *   The Lingotek configuration service.
   * @param \Drupal\lingotek\LanguageLocaleMapperInterface $language_locale_mapper
   *   The language-locale mapper.
   * @param \Drupal\content_translation\ContentTranslationManagerInterface $content_translation_manager
   *   The content translation manager.
   * @param \Drupal\lingotek\LingotekContentTranslationServiceInterface $translation_service
   *   The Lingotek content translation service.
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $temp_store_factory
   *   The factory for the temp store object.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param string $entity_type_id
   *   The entity type id.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle info.
   * @param \Drupal\Core\Utility\LinkGeneratorInterface $link_generator
   *   The link generator.
   * @param \Drupal\lingotek_overrides\FormFilterPluginManager $form_filter_manager
   *   The form-filter plugin manager.
   * @param \Drupal\lingotek_overrides\FormFieldPluginManager $form_field_manager
   *   The form-field plugin manager.
   * @param \Drupal\lingotek_overrides\FormOperationPluginManager $form_operation_manager
   *   The form-operation plugin manager.
   * @param \Drupal\lingotek_overrides\FormOptionPluginManager $form_option_manager
   *   The form-option plugin manager.
   */
  public function __construct(
    Connection $connection,
    EntityTypeManagerInterface $entity_type_manager,
    LanguageManagerInterface $language_manager,
    LingotekInterface $lingotek,
    LingotekConfigurationServiceInterface $lingotek_configuration,
    LanguageLocaleMapperInterface $language_locale_mapper,
    ContentTranslationManagerInterface $content_translation_manager,
    LingotekContentTranslationServiceInterface $translation_service,
    PrivateTempStoreFactory $temp_store_factory,
    StateInterface $state,
    ModuleHandlerInterface $module_handler,
    $entity_type_id,
    EntityFieldManagerInterface $entity_field_manager,
    EntityTypeBundleInfoInterface $entity_type_bundle_info,
    LinkGeneratorInterface $link_generator,
    FormFilterPluginManager $form_filter_manager,
    FormFieldPluginManager $form_field_manager,
    FormOperationPluginManager $form_operation_manager,
    FormOptionPluginManager $form_option_manager
  ) {
    parent::__construct($connection, $entity_type_manager, $language_manager, $lingotek, $lingotek_configuration, $language_locale_mapper, $content_translation_manager, $translation_service, $temp_store_factory, $state, $module_handler, $entity_type_id, $entity_field_manager, $entity_type_bundle_info, $link_generator);
    $form_id = $this->getFormId();
    $this->formFilters = $form_filter_manager->getApplicable($form_id, $this->entityTypeId);
    $this->formFields = $form_field_manager->getApplicable($form_id, $this->entityTypeId);
    $this->formOperations = $form_operation_manager->getApplicable($form_id, $this->entityTypeId);
    $this->formOptions = $form_option_manager->getApplicable($this->formOperations);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('entity_type.manager'),
      $container->get('language_manager'),
      $container->get('lingotek'),
      $container->get('lingotek.configuration'),
      $container->get('lingotek.language_locale_mapper'),
      $container->get('content_translation.manager'),
      $container->get('lingotek.content_translation'),
      $container->get('tempstore.private'),
      $container->get('state'),
      $container->get('module_handler'),
      \Drupal::routeMatch()->getParameter('entity_type_id'),
      $container->get('entity_field.manager'),
      $container->get('entity_type.bundle.info'),
      $container->get('link_generator'),
      $container->get('plugin.manager.lingotek_overrides.form_filter'),
      $container->get('plugin.manager.lingotek_overrides.form_field'),
      $container->get('plugin.manager.lingotek_overrides.form_operation'),
      $container->get('plugin.manager.lingotek_overrides.form_option'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    if ($filters = $form['filters']['wrapper']['lingotek_overrides'] ?? NULL) {
      $form['filters']['advanced_options']['lingotek_overrides'] = $filters;
      unset($form['filters']['wrapper']['lingotek_overrides']);
    }

    $form['filters']['advanced_options']['source_status']['#options'][$this->lingotek::STATUS_UNTRACKED] = $this->t('Untracked');
    $this->activeFilters($form['filters']);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function getFilters() {
    $filters = parent::getFilters();

    if (isset($filters['bundle']) && !$this->hasBundles()) {
      // The parent already checks for this, but not in a satisfactory way, so
      // make sure there is no bundle filter if the entity types has no bundles.
      unset($filters['bundle']);
    }

    if (isset($filters['label']['#placeholder'])) {
      // This is producing the wrong text ("Filter by Content type") so let's
      // just get rid of it. Placeholders are bad for accessibility anyway.
      unset($filters['label']['#placeholder']);
    }

    if (isset($filters['job'])) {
      // Use the JobId filter plugin instead.
      unset($filters['job']);
    }

    if ($this->formFilters) {
      $submitted = $this->tempStoreFactory->get($this->getTempStorageFilterKey())->get('lingotek_overrides');

      foreach ($this->formFilters as $filter_id => $filter) {
        $filters['lingotek_overrides'][$filter_id] = $filter->buildElement($submitted[$filter_id] ?? NULL);
      }
    }

    return $filters;
  }

  /**
   * {@inheritdoc}
   */
  protected function getHeaders() {
    if (is_null($this->headers)) {
      $headers = parent::getHeaders();
      $this->assignWeight($headers, TRUE);
      $entity_type = $this->entityTypeManager->getDefinition($this->entityTypeId);
      // Does a better check than the parent method does.
      if (!$this->hasBundles()) {
        unset($headers['bundle']);
      }
      else {
        // We're not (yet?) overriding default fields with plugins, so we add
        // sorting parameters here instead.
        $headers['bundle']['field'] = 'entity_table.' . $entity_type->getKey('bundle');
      }

      if ($entity_type->hasKey('label')) {
        // Same as for bundle.
        $headers['title']['field'] = 'entity_data.' . $entity_type->getKey('label');
      }

      foreach ($this->formFields as $field_id => $field) {
        $headers[$field_id] = $field->getHeader($this->entityTypeId);
      }

      $headers = array_filter($headers);
      uasort($headers, '\Drupal\Component\Utility\SortArray::sortByWeightElement');
      $this->headers = $headers;
    }

    return $this->headers;
  }

  /**
   * {@inheritdoc}
   */
  protected function getFilteredEntities() {
    $items_per_page = $this->getItemsPerPage();
    $temp_store = $this->tempStoreFactory->get($this->getTempStorageFilterKey());

    /** @var \Drupal\Core\Entity\EntityTypeInterface $entity_type */
    $entity_type = $this->entityTypeManager->getDefinition($this->entityTypeId);
    /** @var \Drupal\Core\Database\Query\PagerSelectExtender $query */
    $query = $this->connection->select($entity_type->getBaseTable(), 'entity_table')->extend('\Drupal\Core\Database\Query\PagerSelectExtender');
    $query->fields('entity_table', [$entity_type->getKey('id')]);

    $query->addTag('lingotek.filter.entity.' . $this->entityTypeId);
    $this->sortableQuery($query, $entity_type);
    $union = NULL;
    $union2 = NULL;

    $has_bundles = $entity_type->get('bundle_entity_type') != 'bundle';

    $groupsExists = $this->moduleHandler->moduleExists('gnode') && $this->entityTypeId === 'node';

    // Filter results
    // Default options
    $labelFilter = $temp_store->get('label');
    $bundleFilter = $temp_store->get('bundle');
    $groupFilter = $groupsExists ? $temp_store->get('group') : NULL;
    $jobFilter = $temp_store->get('job');

    // Advanced options
    $documentIdFilter = $temp_store->get('document_id');
    $entityIdFilter = $temp_store->get('entity_id');
    $sourceLanguageFilter = $temp_store->get('source_language');
    $sourceStatusFilter = $temp_store->get('source_status');
    $targetStatusFilter = $temp_store->get('target_status');
    $contentStateFilter = $temp_store->get('content_state');
    $profileFilter = $temp_store->get('profile');

    if ($sourceStatusFilter) {
      if ($sourceStatusFilter === 'UPLOAD_NEEDED') {
        // We consider that "Upload Needed" includes those never uploaded or
        // disassociated, edited, or with error on last upload.
        $needingUploadStatuses = [
          Lingotek::STATUS_EDITED,
          Lingotek::STATUS_REQUEST,
          Lingotek::STATUS_CANCELLED,
          Lingotek::STATUS_ERROR,
        ];

        // Filter metadata by content_entity_type_id if exists
        $metadata_type = $this->entityTypeManager->getDefinition('lingotek_content_metadata');
        $query->innerJoin($metadata_type->getBaseTable(), 'metadata_source',
          'entity_table.' . $entity_type->getKey('id') . '= metadata_source.content_entity_id AND metadata_source.content_entity_type_id = \'' . $entity_type->id() . '\'');
        $query->innerJoin('lingotek_content_metadata__translation_status', 'translation_status',
          'metadata_source.id = translation_status.entity_id AND translation_status.translation_status_language = entity_table.' . $entity_type->getKey('langcode'));
        $query->condition('translation_status.translation_status_value', $needingUploadStatuses, 'IN');

        // No metadata.
        $no_metadata_query = $this->connection->select($metadata_type->getBaseTable(), 'mt');
        $no_metadata_query->fields('mt', [$metadata_type->getKey('id')]);
        $no_metadata_query->where('entity_table.' . $entity_type->getKey('id') . ' = mt.content_entity_id');
        $no_metadata_query->condition('mt.content_entity_type_id', $entity_type->id());
        $union = $this->connection->select($entity_type->getBaseTable(), 'entity_table');
        $union->fields('entity_table', [$entity_type->getKey('id')]);
        $union->condition('entity_table.' . $entity_type->getKey('langcode'), LanguageInterface::LANGCODE_NOT_SPECIFIED, '!=');
        $union->notExists($no_metadata_query);

        // No target statuses.
        $no_statuses_query = $this->connection->select('lingotek_content_metadata__translation_status', 'tst');
        $no_statuses_query->fields('tst', ['entity_id']);
        $no_statuses_query->where('mt2.' . $metadata_type->getKey('id') . ' = tst.entity_id');
        $union2 = $this->connection->select($entity_type->getBaseTable(), 'entity_table');
        $union2->innerJoin($metadata_type->getBaseTable(), 'mt2',
          'entity_table.' . $entity_type->getKey('id') . '= mt2.content_entity_id AND mt2.content_entity_type_id = \'' . $entity_type->id() . '\'');
        $union2->fields('entity_table', [$entity_type->getKey('id')]);
        $union2->condition('entity_table.' . $entity_type->getKey('langcode'), LanguageInterface::LANGCODE_NOT_SPECIFIED, '!=');
        $union2->notExists($no_statuses_query);

        $query->union($union);
        $query->union($union2);
      }
      elseif ($sourceStatusFilter == $this->lingotek::STATUS_UNTRACKED) {
        $metadata_type = $this->entityTypeManager->getDefinition('lingotek_content_metadata');
        $query->leftJoin($metadata_type->getBaseTable(), 'metadata_source',
          'entity_table.' . $entity_type->getKey('id') . '= metadata_source.content_entity_id AND metadata_source.content_entity_type_id = \'' . $entity_type->id() . '\'');
        $query->leftJoin('lingotek_content_metadata__translation_status', 'translation_status',
          'metadata_source.id = translation_status.entity_id AND translation_status.translation_status_language = entity_table.' . $entity_type->getKey('langcode'));

        $orTranslationStatus = $query->orConditionGroup()
          ->condition('translation_status.translation_status_value', $sourceStatusFilter)
          ->isNull('translation_status.entity_id');

        $query->condition($orTranslationStatus);
      }
      else {
        $metadata_type = $this->entityTypeManager->getDefinition('lingotek_content_metadata');
        $query->innerJoin($metadata_type->getBaseTable(), 'metadata_source',
          'entity_table.' . $entity_type->getKey('id') . '= metadata_source.content_entity_id AND metadata_source.content_entity_type_id = \'' . $entity_type->id() . '\'');
        $query->innerJoin('lingotek_content_metadata__translation_status', 'translation_status',
          'metadata_source.id = translation_status.entity_id AND translation_status.translation_status_language = entity_table.' . $entity_type->getKey('langcode'));
        $query->condition('translation_status.translation_status_value', $sourceStatusFilter);
      }
    }
    // Default queries
    if ($has_bundles && $bundleFilter) {
      if (!in_array("", $bundleFilter, TRUE)) {
        $query->condition('entity_table.' . $entity_type->getKey('bundle'), $bundleFilter, 'IN');
        if ($union !== NULL) {
          $union->condition('entity_table.' . $entity_type->getKey('bundle'), $bundleFilter, 'IN');
        }
        if ($union2 !== NULL) {
          $union2->condition('entity_table.' . $entity_type->getKey('bundle'), $bundleFilter, 'IN');
        }
      }
    }
    if ($labelFilter) {
      $labelKey = $entity_type->getKey('label');
      if ($labelKey) {
        $query->innerJoin($entity_type->getDataTable(), 'entity_data',
          'entity_table.' . $entity_type->getKey('id') . '= entity_data.' . $entity_type->getKey('id'));
        $query->condition('entity_data.' . $labelKey, '%' . $labelFilter . '%', 'LIKE');

        if ($union !== NULL) {
          $union->innerJoin($entity_type->getDataTable(), 'entity_data',
            'entity_table.' . $entity_type->getKey('id') . '= entity_data.' . $entity_type->getKey('id'));
          $union->condition('entity_data.' . $labelKey, '%' . $labelFilter . '%', 'LIKE');
        }
        if ($union2 !== NULL) {
          $union2->innerJoin($entity_type->getDataTable(), 'entity_data',
            'entity_table.' . $entity_type->getKey('id') . '= entity_data.' . $entity_type->getKey('id'));
          $union2->condition('entity_data.' . $labelKey, '%' . $labelFilter . '%', 'LIKE');
        }
      }
    }
    if ($groupFilter) {
      /** @var \Drupal\group\Plugin\GroupContentEnablerManagerInterface $groupContentEnablers */
      $groupType = Group::load($groupFilter)->getGroupType();
      $groupContentEnablers = \Drupal::service('plugin.manager.group_content_enabler');
      $definitions = $groupContentEnablers->getDefinitions();
      $definitions = array_filter($definitions, function ($definition) {
        return ($definition['entity_type_id'] === 'node');
      });
      $valid_values = [];
      foreach ($definitions as $node_definition) {
        $valid_values[] = $groupType->id() . '-' . $node_definition['id'] . '-' . $node_definition['entity_bundle'];
      }
      $query->innerJoin('group_content_field_data', 'group_content',
        'entity_table.' . $entity_type->getKey('id') . '= group_content.entity_id');
      $query->condition('group_content.gid', $groupFilter);
      $query->condition('group_content.type', $valid_values, 'IN');

      if ($union !== NULL) {
        $union->innerJoin('group_content_field_data', 'group_content',
          'entity_table.' . $entity_type->getKey('id') . '= group_content.entity_id');
        $union->condition('group_content.gid', $groupFilter);
        $union->condition('group_content.type', $valid_values, 'IN');
      }
      if ($union2 !== NULL) {
        $union2->innerJoin('group_content_field_data', 'group_content',
          'entity_table.' . $entity_type->getKey('id') . '= group_content.entity_id');
        $union2->condition('group_content.gid', $groupFilter);
        $union2->condition('group_content.type', $valid_values, 'IN');
      }
    }
    if ($jobFilter) {
      /** @var \Drupal\Core\Entity\EntityTypeInterface $metadata_type */
      $metadata_type = $this->entityTypeManager->getDefinition('lingotek_content_metadata');
      $query->innerJoin($metadata_type->getBaseTable(), 'metadata',
        'entity_table.' . $entity_type->getKey('id') . '= metadata.content_entity_id AND metadata.content_entity_type_id = \'' . $entity_type->id() . '\'');
      $query->condition('metadata.job_id', '%' . $jobFilter . '%', 'LIKE');

      if ($union !== NULL) {
        $union->innerJoin($metadata_type->getBaseTable(), 'metadata',
          'entity_table.' . $entity_type->getKey('id') . '= metadata.content_entity_id AND metadata.content_entity_type_id = \'' . $entity_type->id() . '\'');
        $union->condition('metadata.job_id', '%' . $jobFilter . '%', 'LIKE');
      }
      if ($union2 !== NULL) {
        $union2->innerJoin($metadata_type->getBaseTable(), 'metadata',
          'entity_table.' . $entity_type->getKey('id') . '= metadata.content_entity_id AND metadata.content_entity_type_id = \'' . $entity_type->id() . '\'');
        $union2->condition('metadata.job_id', '%' . $jobFilter . '%', 'LIKE');
      }
    }
    // Advanced queries
    if ($documentIdFilter) {
      $documentIdArray = explode(',', $documentIdFilter);
      array_walk($documentIdArray, function (&$value) {
        $value = trim($value);
      });
      $documentIdOperator = (count($documentIdArray) > 1) ? 'IN' : 'LIKE';
      $documentIdValue = (count($documentIdArray) > 1) ? $documentIdArray : '%' . $documentIdFilter . '%';

      $metadata_type = $this->entityTypeManager->getDefinition('lingotek_content_metadata');
      $query->innerJoin($metadata_type->getBaseTable(), 'metadata',
        'entity_table.' . $entity_type->getKey('id') . '= metadata.content_entity_id AND metadata.content_entity_type_id = \'' . $entity_type->id() . '\'');
      $query->condition('metadata.document_id', $documentIdValue, $documentIdOperator);

      if ($union !== NULL) {
        $union->innerJoin($metadata_type->getBaseTable(), 'metadata',
          'entity_table.' . $entity_type->getKey('id') . '= metadata.content_entity_id AND metadata.content_entity_type_id = \'' . $entity_type->id() . '\'');
        $union->condition('metadata.document_id', $documentIdValue, $documentIdOperator);
      }
      if ($union2 !== NULL) {
        $union2->innerJoin($metadata_type->getBaseTable(), 'metadata',
          'entity_table.' . $entity_type->getKey('id') . '= metadata.content_entity_id AND metadata.content_entity_type_id = \'' . $entity_type->id() . '\'');
        $union2->condition('metadata.document_id', $documentIdValue, $documentIdOperator);
      }
    }
    if ($entityIdFilter) {
      $entityIdArray = explode(',', $entityIdFilter);
      array_walk($entityIdArray, function (&$value) {
        $value = trim($value);
      });
      $entityIdOperator = (count($entityIdArray) > 1) ? 'IN' : '=';
      $entityIdValue = (count($entityIdArray) > 1) ? $entityIdArray : $entityIdFilter;

      $query->innerJoin($entity_type->getDataTable(), 'entity_data',
        'entity_table.' . $entity_type->getKey('id') . '= entity_data.' . $entity_type->getKey('id'));

      $query->condition('entity_table.' . $entity_type->getKey('id'), $entityIdValue, $entityIdOperator);

      if ($union !== NULL) {
        $union->innerJoin($entity_type->getDataTable(), 'entity_data',
          'entity_table.' . $entity_type->getKey('id') . '= entity_data.' . $entity_type->getKey('id'));
        $union->condition('entity_table.' . $entity_type->getKey('id'), $entityIdValue, $entityIdOperator);
      }
      if ($union2 !== NULL) {
        $union2->innerJoin($entity_type->getDataTable(), 'entity_data',
          'entity_table.' . $entity_type->getKey('id') . '= entity_data.' . $entity_type->getKey('id'));
        $union2->condition('entity_table.' . $entity_type->getKey('id'), $entityIdValue, $entityIdOperator);
      }
    }
    if ($profileFilter) {
      if (is_string($profileFilter)) {
        $profileFilter = [$profileFilter];
      }
      if (!in_array("", $profileFilter, TRUE)) {
        $metadata_type = $this->entityTypeManager->getDefinition('lingotek_content_metadata');
        $query->innerJoin($metadata_type->getBaseTable(), 'metadata',
          'entity_table.' . $entity_type->getKey('id') . '= metadata.content_entity_id AND metadata.content_entity_type_id = \'' . $entity_type->id() . '\'');
        $query->condition('metadata.profile', $profileFilter, 'IN');

        if ($union !== NULL) {
          $union->innerJoin($metadata_type->getBaseTable(), 'metadata',
            'entity_table.' . $entity_type->getKey('id') . '= metadata.content_entity_id AND metadata.content_entity_type_id = \'' . $entity_type->id() . '\'');
          $union->condition('metadata.profile', $profileFilter, 'IN');
        }
        if ($union2 !== NULL) {
          $union2->innerJoin($metadata_type->getBaseTable(), 'metadata',
            'entity_table.' . $entity_type->getKey('id') . '= metadata.content_entity_id AND metadata.content_entity_type_id = \'' . $entity_type->id() . '\'');
          $union2->condition('metadata.profile', $profileFilter, 'IN');
        }
      }
    }
    if ($sourceLanguageFilter) {
      $query->innerJoin($entity_type->getDataTable(), 'entity_data',
        'entity_table.' . $entity_type->getKey('id') . '= entity_data.' . $entity_type->getKey('id'));
      $query->condition('entity_table.' . $entity_type->getKey('langcode'), $sourceLanguageFilter);
      $query->condition('entity_data.default_langcode', 1);

      if ($union !== NULL) {
        $union->innerJoin($entity_type->getDataTable(), 'entity_data',
          'entity_table.' . $entity_type->getKey('id') . '= entity_data.' . $entity_type->getKey('id'));
        $union->condition('entity_table.' . $entity_type->getKey('langcode'), $sourceLanguageFilter);
        $union->condition('entity_data.default_langcode', 1);
      }
      if ($union2 !== NULL) {
        $union2->innerJoin($entity_type->getDataTable(), 'entity_data',
          'entity_table.' . $entity_type->getKey('id') . '= entity_data.' . $entity_type->getKey('id'));
        $union2->condition('entity_table.' . $entity_type->getKey('langcode'), $sourceLanguageFilter);
        $union2->condition('entity_data.default_langcode', 1);
      }
    }
    // We don't want items with language undefined.
    $query->condition('entity_table.' . $entity_type->getKey('langcode'), LanguageInterface::LANGCODE_NOT_SPECIFIED, '!=');

    if ($targetStatusFilter) {
      /** @var \Drupal\Core\Database\Query\PagerSelectExtender $subquery */
      $subquery = $this->connection->select($entity_type->getBaseTable(), 'entity_table')->extend('\Drupal\Core\Database\Query\PagerSelectExtender');
      $subquery->fields('entity_table', [$entity_type->getKey('id')]);
      $metadata_type = $this->entityTypeManager->getDefinition('lingotek_content_metadata');
      $subquery->innerJoin($metadata_type->getBaseTable(), 'metadata_target',
        'entity_table.' . $entity_type->getKey('id') . '= metadata_target.content_entity_id AND metadata_target.content_entity_type_id = \'' . $entity_type->id() . '\'');
      $subquery->innerJoin('lingotek_content_metadata__translation_status', 'translation_target_status',
       'metadata_target.id = translation_target_status.entity_id AND translation_target_status.translation_status_language <> entity_table.' . $entity_type->getKey('langcode'));
      $subquery->condition('translation_target_status.translation_status_value', $targetStatusFilter);
      $query->condition('entity_table.' . $entity_type->getKey('id'), $subquery, 'IN');

      if ($union !== NULL) {
        $union->condition('entity_table.' . $entity_type->getKey('id'), $subquery, 'IN');
      }
      if ($union2 !== NULL) {
        $union2->condition('entity_table.' . $entity_type->getKey('id'), $subquery, 'IN');
      }
    }

    if ($contentStateFilter != '') {
      $content_moderation_type = $this->entityTypeManager->getDefinition('content_moderation_state');
      $query->innerJoin($content_moderation_type->getDataTable(), 'content_moderation_data',
        'entity_table.' . $entity_type->getKey('id') . '= content_moderation_data.content_entity_id');
      $query->condition('content_moderation_data.moderation_state', $contentStateFilter);

      if ($union !== NULL) {
        $union->innerJoin($content_moderation_type->getDataTable(), 'content_moderation_data',
          'entity_table.' . $entity_type->getKey('id') . '= content_moderation_data.content_entity_id');
        $union->condition('content_moderation_data.moderation_state', $contentStateFilter);
      }
      if ($union2 !== NULL) {
        $union2->innerJoin($content_moderation_type->getDataTable(), 'content_moderation_data',
          'entity_table.' . $entity_type->getKey('id') . '= content_moderation_data.content_entity_id');
        $union2->condition('content_moderation_data.moderation_state', $contentStateFilter);
      }
    }

    if ($overrides = $temp_store->get('lingotek_overrides') ?? []) {
      foreach ($this->cleanFilterValues($overrides) as $filter_id => $filter_value) {
        if ($filter = $this->formFilters[$filter_id] ?? NULL) {
          $filter->filter($this->entityTypeId, [], $filter_value, $query);
        }
      }
    }

    $ids = $query->limit($items_per_page)->execute()->fetchCol(0);
    return $this->entityTypeManager->getStorage($this->entityTypeId)->loadMultiple($ids);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // If the operation doesn't have a submittable callback, let the parent
    // do its job.
    if (!$this->submitFormOperation($form_state)) {
      parent::submitForm($form, $form_state);
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getRow($entity) {
    $row = parent::getRow($entity);
    $this->assignWeight($row);

    if (!$this->hasBundles()) {
      unset($row['bundle']);
    }

    foreach ($this->formFields as $field_id => $field) {
      $row[$field_id] = $field->getData($entity);
    }

    $row = array_filter($row);
    uasort($row, '\Drupal\Component\Utility\SortArray::sortByWeightElement');
    return $row;
  }

  /**
   * {@inheritdoc}
   */
  protected function getFilterKeys() {
    $filter_keys = parent::getFilterKeys();

    foreach ($this->formFilters as $filter) {
      $filter_keys[] = array_merge(['advanced_options', 'lingotek_overrides'], $filter->getFilterKey());
    }

    return $filter_keys;
  }

  /**
   * {@inheritdoc}
   */
  protected function getAllBundles() {
    if ($bundles = parent::getAllBundles()) {
      asort($bundles);
    }

    return $bundles;
  }

  /**
   * Checks whether the entity type has bundles.
   *
   * @return bool
   *   TRUE if the entity type has bundles.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function hasBundles() {
    if (is_null($this->hasBundles)) {
      $entity_type = $this->entityTypeManager->getDefinition($this->entityTypeId);
      $bundle_entity_type = $entity_type->get('bundle_entity_type');
      $this->hasBundles = $bundle_entity_type ? $bundle_entity_type != 'bundle' : FALSE;
    }

    return $this->hasBundles;
  }

  /**
   * Makes a query sortable.
   *
   * @param \Drupal\Core\Database\Query\PagerSelectExtender $query
   *   The query.
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   */
  protected function sortableQuery(PagerSelectExtender $query, EntityTypeInterface $entity_type) {
    /** @var \Drupal\Core\Database\Query\TableSortExtender $table_sort_query */
    // ::distinct() prevents results from being displayed one per page.
    $table_sort_query = $query->distinct()->extend('Drupal\Core\Database\Query\TableSortExtender');
    $table_sort_query->orderByHeader($this->getHeaders());
    // The sorting fields must be added to the select statement so that the
    // query won't choke on ::distinct().
    foreach ($table_sort_query->getOrderBy() as $field => $direction) {
      [$table, $field] = explode('.', $field);
      $table_sort_query->fields($table, [$field]);
    }
    /** @var \Drupal\Core\Database\Query\PagerSelectExtender $query */
    $query = $table_sort_query->extend('Drupal\Core\Database\Query\PagerSelectExtender');
    $query->condition('entity_data.default_langcode', 1);
    $query->innerJoin($entity_type->getDataTable(), 'entity_data',
    'entity_table.' . $entity_type->getKey('id') . '= entity_data.' . $entity_type->getKey('id'));
  }

}
