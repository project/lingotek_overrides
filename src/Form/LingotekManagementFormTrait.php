<?php

namespace Drupal\lingotek_overrides\Form;

use Drupal\Component\Render\MarkupInterface;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;

/**
 * Helper trait for Lingotek-management forms.
 */
trait LingotekManagementFormTrait {

  /**
   * The tempstore factory.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStoreFactory
   */
  protected $tempStoreFactory;

  /**
   * The Lingotek content translation service.
   *
   * @var \Drupal\lingotek\LingotekContentTranslationServiceInterface
   */
  protected $translationService;

  /**
   * Available form-filter plugins.
   *
   * @var \Drupal\lingotek_overrides\Plugin\lingotek_overrides\FormFilterInterface[]
   */
  protected $formFilters = [];

  /**
   * Available form-field plugins.
   *
   * @var \Drupal\lingotek_overrides\Plugin\lingotek_overrides\FormFieldInterface[]
   */
  protected $formFields = [];

  /**
   * Available form-operation plugins.
   *
   * @var \Drupal\lingotek_overrides\Plugin\lingotek_overrides\FormOperationInterface[]
   */
  protected $formOperations = [];

  /**
   * Available form-option plugins.
   *
   * @var \Drupal\lingotek_overrides\Plugin\lingotek_overrides\FormOptionInterface[]
   */
  protected $formOptions = [];

  /**
   * The table's headers.
   *
   * @var array|null
   */
  protected $headers = NULL;

  /**
   * Shows whether any of the advanced filters is active.
   *
   * @param array $filters
   *   The 'filters' form element.
   */
  protected function activeFilters(array &$filters) {
    $advanced = &$filters['advanced_options'];
    $advanced['#title'] = $this->t('Advanced filters');

    foreach (Element::children($advanced) as $filter) {
      if ($value = $this->tempStoreFactory->get($this->getTempStorageFilterKey())->get($filter) ?? NULL) {
        if (is_array($value)) {
          $value = array_filter($value);
        }

        if (!$value) {
          continue;
        }

        $advanced['#title'] = $this->t('Advanced filters [there are active filters]');
        break;
      }
    }
  }

  /**
   * Gets the bulk options form array structure.
   *
   * @return array
   *   A renderable array of form elements.
   *
   * @see \Drupal\lingotek\Form\LingotekManagementFormBase::getBulkOptions()
   */
  protected function getBulkOptions() {
    $element = parent::getBulkOptions();
    unset($element['show_advanced']);
    // Calculate initial weight.
    $weight = $element['operation']['#weight'] = $element['operation']['#weight'] ?? 0;

    foreach ($this->formOptions as $option_id => $option) {
      $_operations = array_keys($option->getOperations());
      $count = count($_operations);
      $states = [];

      foreach ($_operations as $delta => $_operation) {
        $states[] = ['value' => $_operation];

        if ($delta < ($count - 1)) {
          $states[] = 'or';
        }
      }

      $element[$option_id] = $option->buildElement();
      $weight = $element[$option_id]['#weight'] = $weight + $option->getWeight();
      $element[$option_id]['#states'] = [
        'visible' => [
          ':input[name="operation"]'  => $states,
        ],
      ];
    }

    $element['submit']['#weight'] = ++$weight;

    return $element;
  }

  /**
   * Builds the bulk operations for the management form.
   *
   * @return array
   *   Array of bulk operations.
   *
   * @see \Drupal\lingotek\Form\LingotekManagementFormBase::generateBulkOptions()
   */
  public function generateBulkOptions() {
    $operations = [];

    foreach ($this->formOperations as $operation_id => $plugin) {
      $parents = [];

      if ($group = $plugin->getGroup()) {
        $parents[] = $group;
      }

      $parents[] = $operation_id;
      NestedArray::setValue($operations, $parents, $plugin->getTitle());
    }

    return $operations;
  }

  /**
   * Uploads source for translation.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity.
   * @param $language
   *   The language.
   * @param $job_id
   *   The job ID.
   * @param $context
   *   The context.
   */
  public function uploadDocument(ContentEntityInterface $entity, $language, $job_id, &$context) {
    if (!$this->tempStoreFactory->get($this->getTempStoreOptionKey())->get('job_id_overwrite')) {
      if ($existing_job_id = $this->translationService->getJobId($entity)) {
        $job_id = $existing_job_id;
      }
    }

    parent::uploadDocument($entity->getUntranslated(), $language, $job_id, $context);
  }

  /**
   * Generates a weighted data array.
   *
   * @param array $array
   *   The data.
   * @param bool $is_header
   *   If TRUE, markup content will be rendered as a string to prevent errors
   *   when generating sortable headers. Keep this FALSE for table rows to avoid
   *   escaping HTML output.
   */
  protected function assignWeight(array &$array, $is_header = FALSE) {
    $weight = 0;
    array_walk($array, function (&$item) use (&$weight, $is_header) {
      $weight = $weight + 100;

      if (is_array($item) && isset($item['data'])) {
        $item['weight'] = $weight;
      }
      else {
        if ($item instanceof MarkupInterface) {
          $item = $is_header ? $item->__toString() : ['#markup' => $item];
        }

        $item = [
          'data' => $item,
          'weight' => $weight,
        ];
      }
    });
  }

  /**
   * Cleans filter values while preserving literal zeroes.
   *
   * @param mixed $values
   *   The unprocessed values.
   *
   * @return mixed
   *   The cleaned values.
   */
  protected function cleanFilterValues($values) {
    if (is_array($values)) {
      $values = array_filter($values, function ($value) {
        // 0 is valid whether it's an integer or a string.
        return !empty($value) || $value === 0 || $value === '0';
      });
    }

    return $values;
  }

  /**
   * Submits the selected operation.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return bool
   *   If TRUE, the selected operation had a submittable callback.
   */
  protected function submitFormOperation(FormStateInterface $form_state) {
    $operation = $form_state->getValue('operation');
    if ($operation = $this->formOperations[$operation] ?? NULL) {
      // Save option values in the tempstore. Individual plugins may choose
      // not to honor the existing value as their default.
      foreach ($operation->getOptions() as $option_id) {
        $option = $this->formOptions[$option_id];

        foreach ($option->getValueKeys() as $option_key) {
          $this->tempStoreFactory->get($this->getTempStoreOptionKey())->set($option_key, $form_state->getValue($option_key));
        }
      }

      if ($callbacks = $operation->callback($form_state) ?? []) {
        foreach ($callbacks as $arguments) {
          $callback = array_shift($arguments);

          if (!is_array($callback)) {
            $callback = [$this, $callback];
          }

          call_user_func_array($callback, $arguments);
        }
      }
    }

    return !empty($callbacks);
  }

  /**
   * Gets the key used for persisting operation options in the temp storage.
   *
   * @return string
   *   Temp storage identifier.
   */
  protected function getTempStoreOptionKey() {
    return 'lingotek.management.option';
  }

    /**
   * {@inheritDoc}
   */
  protected function getSelectedEntities($values) {
    /** @var \Drupal\Core\Entity\ContentEntityInterface[] $entities */
    $entities = parent::getSelectedEntities($values);

    foreach ($entities as &$entity) {
      $entity = $entity->getUntranslated();
    }

    return $entities;
  }

}
