<?php

namespace Drupal\lingotek_overrides\Form;

use Drupal\config_translation\ConfigEntityMapper;
use Drupal\config_translation\ConfigFieldMapper;
use Drupal\config_translation\ConfigNamesMapper;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\lingotek\Form\LingotekConfigManagementForm as FormLingotekConfigManagementForm;

/**
 * Overrides Lingotek's config-management form.
 */
class LingotekConfigManagementForm extends FormLingotekConfigManagementForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    if ($redirect = $this->checkSetup()) {
      return $redirect;
    }
    $showingFields = FALSE;

    $this->filter = $this->getFilter();
    $temp_store = $this->getFilterTempStore();
    $jobFilter = $temp_store->get('job');

    // Create the headers first so they can be used for sorting.
    $headers = [
      'title' => [
        'data' => $this->t('Entity'),
      ],
      'source' => $this->t('Source'),
      'translations' => $this->t('Translations'),
      'profile' => $this->t('Profile'),
      'job_id' => $this->t('Job ID'),
    ];

    // ToDo: Find a better filter?
    if ($this->filter === 'config') {
      $mappers = array_filter($this->mappers, function ($mapper) {
        return ($mapper instanceof ConfigNamesMapper
          && !$mapper instanceof ConfigEntityMapper
          && !$mapper instanceof ConfigFieldMapper);
      });
    }
    elseif (substr($this->filter, -7) == '_fields') {
      $showingFields = TRUE;
      /** @var \Drupal\config_translation\ConfigFieldMapper $mapper */
      $mapper = $this->mappers[$this->filter];
      $base_entity_type = $mapper->getPluginDefinition()['base_entity_type'];

      // If we are showing field config instances, we need to show bundles for
      // a better UX.
      $headers = [
        'bundle' => [
          'data' => $this->t('Bundle'),
          'specifier' => 'bundle',
        ],
      ] + $headers;

      // Make the table sortable by field label.
      $headers['title']['specifier'] = 'label';

      $query = \Drupal::entityQuery('field_config')
        ->accessCheck(TRUE)
        ->condition('id', $base_entity_type . '.', 'STARTS_WITH')
        ->tableSort($headers);

      if ($label_filter = $temp_store->get('label')) {
        $entity_storage = \Drupal::entityTypeManager()->getStorage($query->getEntityTypeId());
        $entity_keys = $entity_storage->getEntityType()->getKeys();

        if ($label_key = $entity_keys['label'] ?? '') {
          $query->condition($label_key, $label_filter, 'CONTAINS');
        }

      }
      $ids = $query->execute();
      $fields = FieldConfig::loadMultiple($ids);
      $mappers = [];
      foreach ($fields as $id => $field) {
        $new_mapper = clone $mapper;
        $new_mapper->setEntity($field);
        $mappers[$field->id()] = $new_mapper;
      }
    }
    else {
      $mapper = $this->mappers[$this->filter];
      $query = \Drupal::entityQuery($this->filter);
      $query->accessCheck(TRUE);
      $label_filter = $temp_store->get('label');

      // Determine the machine name of the title for this entity type.
      $entity_storage = \Drupal::entityTypeManager()->getStorage($this->filter);
      $entity_keys = $entity_storage->getEntityType()->getKeys();
      if (isset($entity_keys['label'])) {
        $label_key = $entity_keys['label'];
        if ($label_filter) {
          $query->condition($label_key, $label_filter, 'CONTAINS');
        }

        $headers['title']['specifier'] = $label_key;
        $query->tableSort($headers);
      }

      $ids = $query->execute();
      $entities = $entity_storage->loadMultiple($ids);
      /** @var \Drupal\config_translation\ConfigEntityMapper $mapper  */
      $mappers = [];
      foreach ($entities as $entity) {
        $new_mapper = clone $mapper;
        $new_mapper->setEntity($entity);
        $mappers[$entity->id()] = $new_mapper;
      }
    }

    $rows = [];
    foreach ($mappers as $mapper_id => $mapper) {
      if (!in_array($mapper->getLangcode(), [LanguageInterface::LANGCODE_NOT_SPECIFIED, LanguageInterface::LANGCODE_NOT_APPLICABLE])) {
        $is_config_entity = $mapper instanceof ConfigEntityMapper;

        $source = $this->getSourceStatus($mapper);
        $translations = $this->getTranslationsStatuses($mapper);

        // We select those that we want if there is a filter for job ID.
        $job_id = $this->getMetadataJobId($mapper);
        if (!empty($jobFilter)) {
          $found = strpos($job_id, $jobFilter);
          if ($found === FALSE || $found < 0) {
            continue;
          }
        }

        $profile = $is_config_entity ?
          $this->lingotekConfiguration->getConfigEntityProfile($mapper->getEntity()) :
          $this->lingotekConfiguration->getConfigProfile($mapper_id, FALSE);
        $form['table'][$mapper_id] = [
          '#type' => 'checkbox',
          '#value' => $mapper_id,
        ];
        $rows[$mapper_id] = [];
        $title = trim($mapper->getTitle());
        $rows[$mapper_id] += [
          'title' => $title,
          'source' => $source,
          'translations' => $translations,
          'profile' => $profile ? $profile->label() : '',
          'job_id' => $job_id ?: '',
        ];
        if ($is_config_entity) {
          $link = NULL;
          if ($mapper->getEntity()->hasLinkTemplate('canonical')) {
            $link = $mapper->getEntity()->toLink($title);
          }
          elseif ($mapper->getEntity()->hasLinkTemplate('edit-form')) {
            $link = $mapper->getEntity()
              ->toLink($title, 'edit-form');
          }
          if ($link !== NULL) {
            $rows[$mapper_id]['title'] = $link;
          }
        }

        if ($showingFields) {
          $entity_type_id = $mapper->getEntity()->get('entity_type');
          $bundle = $mapper->getEntity()->get('bundle');
          $bundle_info = \Drupal::service('entity_type.bundle.info')
            ->getBundleInfo($entity_type_id);
          if (isset($bundle_info[$bundle])) {
            $rows[$mapper_id]['bundle'] = trim($bundle_info[$bundle]['label']);
          }
          else {
            $rows[$mapper_id]['bundle'] = trim($bundle);
          }
        }
      }
    }
    // Add filters.
    $form['filters'] = [
      '#type' => 'details',
      '#title' => $this->t('Select config bundle'),
      '#open' => TRUE,
      '#weight' => 5,
      '#tree' => TRUE,
    ];
    $form['filters']['wrapper'] = [
      '#type' => 'container',
      '#attributes' => ['class' => ['form--inline', 'clearfix']],
    ];
    $form['filters']['wrapper']['bundle'] = [
      '#type' => 'select',
      '#title' => $this->t('Filter'),
      '#options' => ['config' => $this->t('Simple configuration')] + $this->getAllBundles(),
      '#default_value' => $this->filter,
      '#attributes' => ['class' => ['form-item']],
    ];
    if ($mapper instanceof ConfigEntityMapper) {
      $form['filters']['wrapper']['label'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Label'),
        '#default_value' => $temp_store->get('label'),
        '#attributes' => ['class' => ['form-item']],
      ];
    }
    $form['filters']['wrapper']['job'] = [
      '#type' => 'lingotek_job_id',
      '#title' => $this->t('Job ID'),
      '#default_value' => $jobFilter,
      '#attributes' => ['class' => ['form-item']],
    ];
    $form['filters']['actions'] = [
      '#type' => 'container',
      '#attributes' => ['class' => ['clearfix']],
    ];
    $form['filters']['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Filter'),
      '#submit' => ['::filterForm'],
    ];
    $form['filters']['actions']['reset'] = [
      '#type' => 'submit',
      '#value' => $this->t('Reset'),
      '#submit' => ['::resetFilterForm'],
    ];

    // Build an 'Update options' form.
    $form['options'] = [
      '#type' => 'details',
      '#title' => $this->t('Bulk document management'),
      '#open' => TRUE,
      '#attributes' => ['class' => ['container-inline']],
      '#weight' => 10,
    ];
    $form['options']['operation'] = [
      '#type' => 'select',
      '#title' => $this->t('Action'),
      '#title_display' => 'invisible',
      '#options' => $this->generateBulkOptions(),
    ];
    $form['options']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Execute'),
    ];
    $form['options']['job_id'] = [
      '#type' => 'lingotek_job_id',
      '#title' => $this->t('Job ID'),
      '#description' => $this->t('Assign a job id that you can filter on later on the TMS or in this page.'),
    ];
    $form['table'] = [
      '#header' => $headers,
      '#options' => $rows,
      '#empty' => $this->t('No content available'),
      '#type' => 'tableselect',
      '#weight' => 30,
    ];
    $form['pager'] = [
      '#type' => 'pager',
      '#weight' => 50,
    ];
    $form['#attached']['library'][] = 'lingotek/lingotek';
    return $form;
  }

  /**
   * Overrides parent to sort bundles by label.
   *
   * @return array
   *   The sorted bundles.
   */
  protected function getAllBundles() {
    $mappers = array_filter($this->mappers, function ($mapper) {
      // Filter config entity mappers and config field mappers.
      return ($mapper instanceof ConfigEntityMapper);
    });
    $bundles = [];
    foreach ($mappers as $bundle => $mapper) {
      /** @var \Drupal\config_translation\ConfigEntityMapper $mapper */
      $bundles[$bundle] = (string) $mapper->getTypeLabel();
    }
    asort($bundles);
    return $bundles;
  }

}
