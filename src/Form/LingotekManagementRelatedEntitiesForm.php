<?php

namespace Drupal\lingotek_overrides\Form;

use Drupal\content_translation\ContentTranslationManagerInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Pager\PagerManagerInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\Core\Utility\LinkGeneratorInterface;
use Drupal\lingotek\Form\LingotekManagementRelatedEntitiesForm as BaseLingotekManagementRelatedEntitiesForm;
use Drupal\lingotek\LanguageLocaleMapperInterface;
use Drupal\lingotek\LingotekContentTranslationServiceInterface;
use Drupal\lingotek\LingotekInterface;
use Drupal\lingotek\RelatedEntities\RelatedEntitiesDetectorManager;
use Drupal\lingotek_overrides\FormFieldPluginManager;
use Drupal\lingotek_overrides\FormFilterPluginManager;
use Drupal\lingotek_overrides\FormOperationPluginManager;
use Drupal\lingotek_overrides\FormOptionPluginManager;
use Drupal\lingotek_overrides\LingotekConfigurationServiceInterface;
use Drupal\lingotek_overrides\RelatedEntities\RestrictedEntitiesDetectorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Overrides an entity's Lingotek-management form.
 *
 * @package Drupal\lingotek_overrides\Form
 */
class LingotekManagementRelatedEntitiesForm extends BaseLingotekManagementRelatedEntitiesForm {

  use LingotekManagementFormTrait;

  /**
   * Override for lingotek.configuration service.
   *
   * @var \Drupal\lingotek_overrides\LingotekConfigurationServiceInterface
   */
  protected $lingotekConfiguration;

  /**
   * The pager.manager service.
   *
   * @var \Drupal\Core\Pager\PagerManagerInterface
   */
  protected $pagerManager;

  /**
   * The plugin.manager.related_entities_detector service.
   *
   * @var \Drupal\lingotek\RelatedEntities\RelatedEntitiesDetectorManager
   */
  protected $relatedEntitiesDetectorManager;

  /**
   * LingotekManagementRelatedEntitiesForm constructor.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The current database connection.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity manager.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\lingotek\LingotekInterface $lingotek
   *   The lingotek service.
   * @param \Drupal\lingotek\LingotekConfigurationServiceInterface $lingotek_configuration
   *   The Lingotek configuration service.
   * @param \Drupal\lingotek\LanguageLocaleMapperInterface $language_locale_mapper
   *   The language-locale mapper.
   * @param \Drupal\content_translation\ContentTranslationManagerInterface $content_translation_manager
   *   The content translation manager.
   * @param \Drupal\lingotek\LingotekContentTranslationServiceInterface $translation_service
   *   The Lingotek content translation service.
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $temp_store_factory
   *   The factory for the temp store object.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle info.
   * @param \Drupal\Core\Utility\LinkGeneratorInterface $link_generator
   *   The link generator.
   * @param \Drupal\Core\Pager\PagerManagerInterface $pager_manager
   *   The pager.manager service.
   * @param \Drupal\lingotek_overrides\FormFilterPluginManager $form_filter_manager
   *   The form-filter plugin manager.
   * @param \Drupal\lingotek_overrides\FormFieldPluginManager $form_field_manager
   *   The form-field plugin manager.
   * @param \Drupal\lingotek_overrides\FormOperationPluginManager $form_operation_manager
   *   The form-operation plugin manager.
   * @param \Drupal\lingotek_overrides\FormOptionPluginManager $form_option_manager
   *   The form-option plugin manager.
   * @param \Drupal\lingotek\RelatedEntities\RelatedEntitiesDetectorManager $related_entities_detector_manager
   *   The plugin.manager.related_entities_detector service.
   */
  public function __construct(
    Connection $connection,
    EntityTypeManagerInterface $entity_type_manager,
    LanguageManagerInterface $language_manager,
    LingotekInterface $lingotek,
    LingotekConfigurationServiceInterface $lingotek_configuration,
    LanguageLocaleMapperInterface $language_locale_mapper,
    ContentTranslationManagerInterface $content_translation_manager,
    LingotekContentTranslationServiceInterface $translation_service,
    PrivateTempStoreFactory $temp_store_factory,
    StateInterface $state,
    ModuleHandlerInterface $module_handler,
    EntityFieldManagerInterface $entity_field_manager = NULL,
    EntityTypeBundleInfoInterface $entity_type_bundle_info = NULL,
    LinkGeneratorInterface $link_generator = NULL,
    PagerManagerInterface $pager_manager,
    FormFilterPluginManager $form_filter_manager,
    FormFieldPluginManager $form_field_manager,
    FormOperationPluginManager $form_operation_manager,
    FormOptionPluginManager $form_option_manager,
    RelatedEntitiesDetectorManager $related_entities_detector_manager
  ) {
    parent::__construct($connection, $entity_type_manager, $language_manager, $lingotek, $lingotek_configuration, $language_locale_mapper, $content_translation_manager, $translation_service, $temp_store_factory, $state, $module_handler, $entity_field_manager, $entity_type_bundle_info, $link_generator);
    $this->pagerManager = $pager_manager;
    $form_id = $this->getFormId();
    $this->formFilters = $form_filter_manager->getApplicable($form_id);
    $this->formFields = $form_field_manager->getApplicable($form_id, $this->entityTypeId);
    $this->formOperations = $form_operation_manager->getApplicable($form_id);
    $this->formOptions = $form_option_manager->getApplicable($this->formOperations);
    $this->relatedEntitiesDetectorManager = $related_entities_detector_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('entity_type.manager'),
      $container->get('language_manager'),
      $container->get('lingotek'),
      $container->get('lingotek.configuration'),
      $container->get('lingotek.language_locale_mapper'),
      $container->get('content_translation.manager'),
      $container->get('lingotek.content_translation'),
      $container->get('tempstore.private'),
      $container->get('state'),
      $container->get('module_handler'),
      $container->get('entity_field.manager'),
      $container->get('entity_type.bundle.info'),
      $container->get('link_generator'),
      $container->get('pager.manager'),
      $container->get('plugin.manager.lingotek_overrides.form_filter'),
      $container->get('plugin.manager.lingotek_overrides.form_field'),
      $container->get('plugin.manager.lingotek_overrides.form_operation'),
      $container->get('plugin.manager.lingotek_overrides.form_option'),
      $container->get('plugin.manager.related_entities_detector')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, ContentEntityInterface $node = NULL) {
    if ($restrict = $this->getRestrictRelated()) {
      $this->setRecursionDepth(100);
    }

    $form = parent::buildForm($form, $form_state, $node);

    if ($restrict) {
      unset($form['depth_selection']);
    }

    if ($filters = $form['filters']['wrapper']['lingotek_overrides'] ?? NULL) {
      $form['filters']['advanced_options']['lingotek_overrides'] = $filters;
      unset($form['filters']['wrapper']['lingotek_overrides']);
    }

    $form['filters']['advanced_options']['source_status']['#options'][$this->lingotek::STATUS_UNTRACKED] = $this->t('Untracked');
    $this->activeFilters($form['filters']);

    $items_per_page = $this->getItemsPerPage();
    // Add select-all checkbox and pager if necessary.
    if (count($form['table']['#options']) > $items_per_page) {
      $form['header']['#weight'] = 20;
      $form['header']['select_all'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Select / deselect all results (all pages, @count total)', [
          '@count' => count($form['table']['#options']),
        ]),
        '#attributes' => ['class' => ['lingotek-overrides-select-all']],
      ];
      // Store info on all entities before slicing the array.
      $form['all_rows'] = [
        '#type' => 'value',
        '#value' => $form['table']['#options'],
      ];

      $pager = $this->pagerManager->createPager(count($form['table']['#options']), $items_per_page);

      $form['pager'] = [
        '#type' => 'pager',
        '#weight' => 50,
      ];

      $form['table']['#attached']['library'][] = 'lingotek_overrides/admin';
      $form['table']['#attributes']['class'][] = 'lingotek-overrides-tableselect';
      $form['table']['#options'] = array_slice($form['table']['#options'], $pager->getCurrentPage() * $items_per_page, $items_per_page);
    }

    if (empty($form['related']['table']['#rows'])) {
      unset($form['related']);
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if ($form_state->getValue(['select_all'])) {
      $rows = array_keys($form_state->getValue(['all_rows']));
      $form_state->setValue(['table'], array_combine($rows, $rows));
    }
    // If the operation doesn't have a submittable callback, let the parent
    // do its job.
    if (!$this->submitFormOperation($form_state)) {
      parent::submitForm($form, $form_state);
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getFilters() {
    $filters['restrict_related'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Restrict related entities'),
      '#description' => $this->t('Recursively shows only entities referenced through fields that have the “Always show related entities” setting enabled.'),
      '#default_value' => $this->getRestrictRelated(),
    ];

    if ($this->formFilters) {
      $submitted = $this->tempStoreFactory->get($this->getTempStorageFilterKey())->get('lingotek_overrides');

      foreach ($this->formFilters as $filter_id => $filter) {
        $filters['lingotek_overrides'][$filter_id] = $filter->buildElement($submitted[$filter_id] ?? NULL);
      }
    }

    return $filters;
  }

  /**
   * {@inheritdoc}
   */
  protected function getFilterKeys() {
    // We need specific identifiers for default and advanced filters since the
    // advanced filters bundle is unique.
    $filtersKeys = [
      ['wrapper', 'restrict_related'],
      ['advanced_options', 'document_id'],
      ['advanced_options', 'entity_id'],
      ['advanced_options', 'profile'],
      ['advanced_options', 'source_language'],
      ['advanced_options', 'source_status'],
      ['advanced_options', 'target_status'],
      ['advanced_options', 'content_state'],
    ];

    if ($this->moduleHandler->moduleExists('gnode') && $this->entityTypeId === 'node') {
      $filtersKeys[] = ['wrapper', 'group'];
    }

    foreach ($this->formFilters as $filter) {
      $filtersKeys[] = array_merge(['advanced_options', 'lingotek_overrides'], $filter->getFilterKey());
    }

    return $filtersKeys;
  }

  /**
   * {@inheritdoc}
   */
  protected function getTempStorageFilterKey() {
    return 'lingotek.management.filter.node';
  }

  /**
   * {@inheritdoc}
   */
  protected function getHeaders() {
    if (is_null($this->headers)) {
      $headers = parent::getHeaders();
      // Correct terminology in parent.
      $headers['entity_type_id'] = $this->t('Entity Type');
      $this->assignWeight($headers, TRUE);

      foreach ($this->formFields as $field_id => $field) {
        $headers[$field_id] = $field->getHeader($this->entityTypeId);
      }

      $headers = array_filter($headers);
      uasort($headers, '\Drupal\Component\Utility\SortArray::sortByWeightElement');
      $this->headers = $headers;
    }

    return $this->headers;
  }

  /**
   * {@inheritdoc}
   *
   * Because the parent method doesn't perform a SQL query, this is a highly
   * inefficient way to filter entities, but given what this form is doing it's
   * the probably the best we can do for now.
   */
  protected function getFilteredEntities() {
    $restrict = $this->getRestrictRelated();
    $this->lingotekConfiguration->setRestrictFilter($restrict);
    // Replace parent::getFilteredEntities() completely.
    /** @var \Drupal\Core\Entity\ContentEntityInterface[][] $entities */
    $entities = [];
    $related = [];
    $visited = [];
    $recursion_depth = $this->getRecursionDepth();
    $plugin_definitions = $this->relatedEntitiesDetectorManager->getDefinitions();
    uasort($plugin_definitions, 'Drupal\Component\Utility\SortArray::sortByWeightElement');
    foreach ($plugin_definitions as $plugin_definition_id => $plugin_definition) {
      /** @var \Drupal\lingotek\RelatedEntities\RelatedEntitiesDetectorInterface $plugin */
      $plugin = $this->relatedEntitiesDetectorManager->createInstance($plugin_definition_id, []);

      if ($restrict && !($plugin instanceof RestrictedEntitiesDetectorInterface)) {
        continue;
      }

      if (!$restrict && ($plugin instanceof RestrictedEntitiesDetectorInterface)) {
        continue;
      }

      $entities = $plugin->extract($this->node, $entities, $related, $recursion_depth, $visited);
    }
    $this->related = $related;

    $temp_store = $this->tempStoreFactory->get($this->getTempStorageFilterKey());
    $filters = [];
    $document_ids = [];
    $entity_ids = [];

    foreach ($this->getFilterKeys() as $key) {
      $filter = $temp_store->get($key[1]);
      $filters[$key[1]] = $this->cleanFilterValues($filter);
    }

    $filters = array_filter($filters);

    if ($_document_ids = preg_replace("/\s+/", '', $filters['document_id'] ?? '')) {
      $document_ids = explode(',', $_document_ids);
    }

    if ($filters['entity_id'] ?? NULL) {
      $_entity_ids = preg_replace("/\s+/", '', $filters['entity_id']);
      $entity_ids = explode(',', $_entity_ids);
    }

    foreach ($entities as $entity_type => &$_entities) {
      if ($entity_ids) {
        $_entities = array_intersect_key($_entities, array_flip($entity_ids));
      }
      /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
      foreach ($_entities as $id => $entity) {
        if ($document_ids) {
          if (!in_array($this->translationService->getDocumentId($entity), $document_ids)) {
            unset($_entities[$id]);
            continue;
          }
        }

        if ($source_language = $filters['source_language'] ?? '') {
          if ($source_language != $entity->getUntranslated()->language()->getId()) {
            unset($_entities[$id]);
            continue;
          }
        }

        if ($source_status = $filters['source_status'] ?? '') {
          if ($source_status != $this->translationService->getSourceStatus($entity)) {
            unset($_entities[$id]);
            continue;
          }
        }

        if ($target_status = $filters['target_status'] ?? '') {
          $target_statuses = $this->translationService->getTargetStatuses($entity);

          if (!in_array($target_status, $target_statuses)) {
            unset($_entities[$id]);
            continue;
          }
        }

        if ($content_state = $filters['content_state'] ?? '') {
          if ($entity->hasField('moderation_state')) {
            $count = 0;

            foreach (array_keys($entity->getTranslationLanguages()) as $langcode) {
              $translation = $entity->getTranslation($langcode);

              if ($translation->get('moderation_state')->getString() == $content_state) {
                $count++;
              }
            }

            if (!$count) {
              unset($_entities[$id]);
              continue;
            }
          }
        }

        if ($profile = $filters['profile'] ?? []) {
          if (!in_array($this->lingotekConfiguration->getEntityProfile($entity)->id(), $profile)) {
            unset($_entities[$id]);
            continue;
          }
        }
      }

      if ($_entities && ($overrides = $filters['lingotek_overrides'] ?? [])) {
        /** @var \Drupal\Core\Database\Query\SelectInterface $query */
        $query = NULL;

        foreach ($overrides as $filter_id => $filter_value) {
          if ($filter = $this->formFilters[$filter_id] ?? NULL) {
            $filter->filter($entity_type, $_entities, $filter_value, $query);
          }
        }

        if ($query ?? NULL) {
          $_entities = array_intersect_key($_entities, $query->execute()->fetchAllKeyed());
        }
      }
    }

    return array_filter($entities);
  }

  /**
   * {@inheritdoc}
   */
  protected function getRow($entity) {
    $row = parent::getRow($entity);
    $this->assignWeight($row);

    foreach ($this->formFields as $field_id => $field) {
      $row[$field_id] = $field->getData($entity);
    }

    $row = array_filter($row);
    uasort($row, '\Drupal\Component\Utility\SortArray::sortByWeightElement');
    return $row;
  }

  /**
   * Checks whether the restrict filter is active.
   *
   * @return int
   *   The recursion depth.
   */
  protected function getRestrictRelated() {
    static $restrict;

    if (is_null($restrict)) {
      $temp_store = $this->tempStoreFactory->get($this->getTempStorageFilterKey());
      $restrict = $temp_store->get('restrict_related');

      if (is_null($restrict)) {
        $restrict = TRUE;
      }
      else {
        $restrict = (bool) $restrict;
      }
    }

    return $restrict;
  }

  /**
   * {@inheritdoc}
   */
  protected function getItemsPerPage() {
    return 25;
  }

}
