<?php

namespace Drupal\lingotek_overrides;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Manages Lingotek form-field plugins.
 *
 * @see \Drupal\lingotek_overrides\Plugin\lingotek_overrides\FormFieldInterface
 * @see \Drupal\lingotek_overrides\Plugin\lingotek_overrides\FormFieldBase
 * @see \Drupal\lingotek_overrides\Annotation\LingotekOverridesFormField
 * @see \hook_lingotek_overrides_form_field_alter()
 */
class FormFieldPluginManager extends FormPluginManagerBase {

  /**
   * FormFieldPluginManager constructor.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   The cache.discovery service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module_handler service.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/lingotek_overrides/FormField', $namespaces, $module_handler, 'Drupal\lingotek_overrides\Plugin\lingotek_overrides\FormFieldInterface', 'Drupal\lingotek_overrides\Annotation\LingotekOverridesFormField');

    $this->alterInfo('lingotek_overrides_form_field');
    $this->setCacheBackend($cache_backend, 'lingotek_overrides_form_field');
  }

}
