/**
 * @file
 * Lingotek Overrides admin UI functionality.
 */

(function ($, Drupal) {

  'use strict';

  /**
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.lingotekOverrides = {
    attach: function (context, settings) {
      $('.lingotek-overrides-select-all').once('lingotek-overrides-admin').each(Drupal.LingotekOverridesSelectAll);
      $('.lingotek-overrides-tableselect input[type="checkbox"]').each(Drupal.LingotekOverridesDeselectSingle);
    }
  };

  /**
   * Checks/unchecks all checkboxes when "Select all" is checked/unchecked.
   */
  Drupal.LingotekOverridesSelectAll = function () {
    $(this).on('click', function (event) {
      var $checkboxes = $('.lingotek-overrides-tableselect input[type="checkbox"]');

      $checkboxes.each(function () {
        $(this).prop('checked', event.target.checked).trigger('change');
        $(this).closest('tr').toggleClass('selected', this.checked);
      });
    });
  };

  /**
   * Unchecks "Select all" when one checkbox is unchecked.
   */
  Drupal.LingotekOverridesDeselectSingle = function () {
    console.log($(this));
    $(this).on('click', function (event) {
      var $checkbox = $('.lingotek-overrides-select-all'),
        allChecked = $checkbox.prop('checked');

      if (allChecked) {
        $checkbox.each(function () {
          var state = $checkbox.prop('checked');

          $(this).prop('checked', !state).trigger('change');
        });
      }
    });
  };

})(jQuery, Drupal);
